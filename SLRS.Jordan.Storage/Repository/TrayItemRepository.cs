﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;

namespace SLRS.Jordan.Storage.Repository
{
    public class TrayItemRepository : Repository<TrayItem>, ITrayItemRepository
    {
        public TrayItemRepository(JordanContext context)
            : base(context)
        {

        }

        //public ICollection<TrayItem> GetItemsByTray(int id)
        //{

        //    return dbContext.TrayItems.Where(t => t.TrayId == id)
        //        .Include(t => t.Article)
        //        .GroupBy(t => t.ArticleId)
        //        .Select(g => new TrayItem
        //        {
        //            Article = g.First().Article,
        //            Id = g.First().Id,
        //            Quantity = g.Sum(t => t.Quantity),
        //            ArticleId = g.First().ArticleId,
        //            TrayId = g.First().TrayId
        //        })
        //        .ToList();
        //}
    }
}
