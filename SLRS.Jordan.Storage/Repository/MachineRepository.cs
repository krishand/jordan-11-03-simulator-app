﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;

namespace SLRS.Jordan.Storage.Repository
{
    public class MachineRepository: Repository<Machine>, IMachineRepository
    {
        public MachineRepository(JordanContext context)
            : base(context)
        {

        }
    }
}
