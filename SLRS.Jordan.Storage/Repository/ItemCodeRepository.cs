﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Storage.Repository
{
    public class ItemCodeRepository : Repository<ItemCode>, IItemCodeRepository
    {

        public ItemCodeRepository(JordanContext contex)
            :base(contex)
        {

        }
    }
}
