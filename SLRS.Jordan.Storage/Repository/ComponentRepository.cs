﻿using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;

namespace SLRS.Jordan.Storage.Repository
{
    public class ComponentRepository : Repository<Component>, IComponentRepository
    {
        public ComponentRepository(JordanContext context)
            : base(context)
        {

        }
    }
}
