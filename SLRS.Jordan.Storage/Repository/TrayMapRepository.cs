﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;

namespace SLRS.Jordan.Storage.Repository
{
    public class TrayMapRepository : Repository<TrayMap>, ITrayMapRepository
    {
        public TrayMapRepository(JordanContext context)
            : base(context)
        {

        }
    }
}
