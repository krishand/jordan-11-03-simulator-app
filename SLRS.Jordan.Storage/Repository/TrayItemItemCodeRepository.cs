﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Storage.Repository
{
    public class TrayItemItemCodeRepository : Repository<TrayItemItemCode>, ITrayItemItemCodeRepository
    {

        public TrayItemItemCodeRepository(JordanContext context) 
            : base(context)
        {

        }

    }
}
