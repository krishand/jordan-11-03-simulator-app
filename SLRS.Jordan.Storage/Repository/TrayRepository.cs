﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;

namespace SLRS.Jordan.Storage.Repository
{
    public class TrayRepository : Repository<Tray>, ITrayRepository
    {
        public TrayRepository(JordanContext context)
            : base(context)
        {

        }
    }
}
