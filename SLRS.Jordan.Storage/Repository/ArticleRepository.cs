﻿using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SLRS.Jordan.Storage.Repository
{
    public class ArticleRepository: Repository<Article>, IArticleRepository
    {
        public ArticleRepository(JordanContext context)
            : base(context)
        {

        }

        public override Task<Article> GetById(int id)
        {
            return dbContext.Articles.Include(i => i.ArticleComponents).FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}
