﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository.Interfaces;
using SLRS.Jordan.Core.DTO;
using System;
using SLRS.Jordan.Core.Models.Articles;

namespace SLRS.Jordan.Storage.Repository
{
    public class RequestOrderRepository : Repository<RequestOrder>, IRequestOrderRepository
    {
        public RequestOrderRepository(JordanContext context)
            : base(context)
        {

        }

        public override Task<RequestOrder> GetById(int id)
        {
            return dbContext.RequestOrders.Include(i => i.OrderItems).FirstOrDefaultAsync(i => i.Id == id);
        }

        public object GetRequestOrderGroupByMachine(int requestOrderId)
        {
            //assumption
            //a request order can not have different order items to same article
            //if user made such a request in needs to merge into one order item

            //get the request order
            var requestOrder = dbContext.RequestOrders
               .Where(t => t.Id == requestOrderId)
               .Include(t => t.OrderItems).ThenInclude(t => t.Article)
               .Include(t => t.Component)
               .FirstOrDefault();

            var articleIds = requestOrder.OrderItems.Select(t => t.ArticleId).ToList();

            //select trayItems that contain requested articles
            //first group by tray id
            //inside that group by article
            var trayItemGroup = (from trayItem in dbContext.TrayItems
                                 where articleIds.Contains(trayItem.ArticleId) && (trayItem.Status == RequestStatus.Loaded || trayItem.Status == RequestStatus.Returned) 
                                 group trayItem by trayItem.TrayId into trayGroup
                                 select new TrayItemGroupByTray()
                                 {
                                     TrayId = trayGroup.First().Tray.Number,
                                     ArticleList = trayGroup
                                     .GroupBy(t => t.ArticleId)
                                     .Select(t => new TrayItemGroupByArticleWithTotalQuanity(requestOrder.OrderItems.Where(t2 => t2.ArticleId == t.Key).First().Quantity)
                                     {
                                         ArticleId = t.Key,
                                         TotalQuantity = t.Sum(t2 => t2.Quantity),
                                         TrayItemList = t.ToList(),

                                     }).ToList()
                                 });

            var trayReference = new List<TrayReference>();
            IEnumerable<int> restOfArticleIds;

            //firt filter: similar article in one tray
            foreach (var orderItem in requestOrder.OrderItems)
            {
                foreach (var trayItemGroupByTray in trayItemGroup)
                {
                    restOfArticleIds = requestOrder.OrderItems.Select(t => t.ArticleId).Except(trayReference.Select(t => t.ArticleId));

                    if (trayItemGroupByTray.ArticleList.Where(t => restOfArticleIds.Contains(t.ArticleId)).Count() > 1)
                    {
                        foreach (var trayItemGroupByArticle in trayItemGroupByTray.ArticleList)
                        {
                            if (trayItemGroupByArticle.CanFulfillRequestedQuantity)
                            {
                                if (trayReference.Where(t => t.ArticleId == trayItemGroupByArticle.ArticleId).Count() == 0)
                                {
                                    var article = dbContext.Articles.Find(trayItemGroupByArticle.ArticleId);

                                    var newTrayReference = new TrayReference()
                                    {
                                        ArticleId = trayItemGroupByArticle.ArticleId,
                                        TrayId = trayItemGroupByTray.TrayId,
                                        Article = article,
                                        RequestedQuantity = orderItem.Quantity
                                    };

                                    trayReference.Add(newTrayReference);
                                }
                            }
                        }
                    }

                }

                //if everything collected, no longer need to run the loop
                if (trayReference.Count == articleIds.Count)
                {
                    break;
                }
            }

            //second filter: rest of the article divided by machine
            //if first filter not collected all
            if (trayReference.Count < articleIds.Count)
            {
                restOfArticleIds = requestOrder.OrderItems.Select(t => t.ArticleId).Except(trayReference.Select(t => t.ArticleId));
                
                foreach (var articleId in restOfArticleIds)
                {
                    var traysWithArticle = trayItemGroup.Where(t => t.ArticleList.Where(t1 => t1.ArticleId == articleId && t1.CanFulfillRequestedQuantity).Any());
                    var article = dbContext.Articles.Find(articleId);
                    var orderItem = requestOrder.OrderItems.Where(t => t.ArticleId == article.Id).First();

                    var machine1Trays = traysWithArticle.Where(t => t.TrayId <= 60 || t.TrayId > 120).ToList();
                    var machine2Trays = traysWithArticle.Where(t => t.TrayId >= 60 && t.TrayId < 120).ToList();

                    //if we can get from both machine
                    if (machine1Trays.Count() > 0 && machine2Trays.Count() > 0)
                    {
                        var currentMachine1TrayCount = trayReference.Where(t => t.TrayId <= 60 || t.TrayId > 120).Count();
                        var currentMachine2TrayCount = trayReference.Where(t => t.TrayId >= 60 && t.TrayId < 120).Count();

                        if (currentMachine1TrayCount > currentMachine2TrayCount)
                        {
                            var newTrayReference = new TrayReference()
                            {
                                ArticleId = articleId,
                                TrayId = machine1Trays.First().TrayId,
                                Article = article,
                                RequestedQuantity = orderItem.Quantity
                            };

                            trayReference.Add(newTrayReference);
                        }
                        else
                        {
                            var newTrayReference = new TrayReference()
                            {
                                ArticleId = articleId,
                                TrayId = machine2Trays.First().TrayId,
                                Article = article,
                                RequestedQuantity = orderItem.Quantity
                            };

                            trayReference.Add(newTrayReference);
                        }
                    }
                    else if (machine1Trays.Count() > 0)
                    {
                        var newTrayReference = new TrayReference()
                        {
                            ArticleId = articleId,
                            TrayId = machine1Trays.First().TrayId,
                            Article = article,
                            RequestedQuantity = orderItem.Quantity
                        };

                        trayReference.Add(newTrayReference);

                    }
                    else if (machine2Trays.Count() > 0)
                    {
                        var newTrayReference = new TrayReference()
                        {
                            ArticleId = articleId,
                            TrayId = machine2Trays.First().TrayId,
                            Article = article,
                            RequestedQuantity = orderItem.Quantity
                        };

                        trayReference.Add(newTrayReference);
                    }
                }
            }

            //third filter: ignore the quantity get from any tray that article contain with maximum number
            restOfArticleIds = requestOrder.OrderItems.Select(t => t.ArticleId).Except(trayReference.Select(t => t.ArticleId));
            if (restOfArticleIds.Count() > 0)
            {
                foreach (var article in restOfArticleIds)
                {
                    //TODO

                    //check do we already open a tray that contain this article

                    //if not open some
                }
            }


            //finalizing result
            var trayReferenceGroupByTray = trayReference.GroupBy(t => t.TrayId);
            var fmachine1Trays = trayReferenceGroupByTray.Where(t => t.Key <= 60 || t.Key > 120).ToList();
            var fmachine2Trays = trayReferenceGroupByTray.Where(t => t.Key >= 60 && t.Key < 120).ToList();


            var x = new TrayItemGroupByMachine()
            {
                Machine1 = fmachine1Trays.Select(t => new TrayReferenceGroup() { TrayId = t.Key, TrayReference = t.ToList() }).ToList(),
                Machine2 = fmachine2Trays.Select(t => new TrayReferenceGroup() { TrayId = t.Key, TrayReference = t.ToList() }).ToList()
            };

            return x;
        }

        public int GetAvailableArticleQuantity(Article article)
        {
            var totalStock = dbContext.TrayItems.Where(t => t.Article == article).Sum(t => t.Quantity);
            var totalRequested = dbContext.OrderItems.Where(t => t.Article == article).Sum(t => t.Quantity);

            return totalStock - totalRequested;
        }
    }

}
