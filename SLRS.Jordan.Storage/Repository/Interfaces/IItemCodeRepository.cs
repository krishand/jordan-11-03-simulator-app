﻿using SLRS.Jordan.Core.Models.LogiMat;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Storage.Repository.Interfaces
{
    public interface IItemCodeRepository : IRepository<ItemCode>
    {

    }
}
