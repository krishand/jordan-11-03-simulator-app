﻿using SLRS.Jordan.Core.Models.Articles;

namespace SLRS.Jordan.Storage.Repository.Interfaces
{
    public interface IComponentRepository : IRepository<Component>
    {
    }
}
