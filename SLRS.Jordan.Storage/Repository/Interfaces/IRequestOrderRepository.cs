﻿using System.Linq;
using System.Threading.Tasks;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Orders;

namespace SLRS.Jordan.Storage.Repository.Interfaces
{
    public interface IRequestOrderRepository : IRepository<RequestOrder>
    {
        object GetRequestOrderGroupByMachine(int requestOrderId);
    }
}
