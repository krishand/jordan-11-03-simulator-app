﻿using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.LogiMat;
using System.Collections.Generic;

namespace SLRS.Jordan.Storage.Repository.Interfaces
{
    public interface ITrayItemRepository: IRepository<TrayItem>
    {

    }
}
