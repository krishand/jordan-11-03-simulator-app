﻿using SLRS.Jordan.Core.Models.LogiMat;

namespace SLRS.Jordan.Storage.Repository.Interfaces
{
    public interface IMachineRepository: IRepository<Machine>
    {
    }
}
