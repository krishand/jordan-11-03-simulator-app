﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Logs;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.Storage.Models;

namespace SLRS.Jordan.Storage.Context
{
    public class JordanContext: IdentityDbContext
    {
        public JordanContext(DbContextOptions<JordanContext> options)
            : base(options)
        {
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Tray> Trays { get; set; }
        public DbSet<TrayItem> TrayItems { get; set; }
        public DbSet<TrayMap> TrayMaps { get; set; }
        public DbSet<RequestOrder> RequestOrders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<ItemCode> ItemCodes { get; set; }
        public DbSet<TrayItemItemCode> TrayItemItemCodes { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Article>(entity =>
            {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<Component>(entity =>
            {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<Machine>(entity =>
            {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<ItemCode>(entity =>
            {
                entity.Property(e => e.RfidCode).HasMaxLength(256);
                entity.HasIndex(e => e.RfidCode).IsUnique();
            });

            builder.Entity<ArticleComponent>(entity =>
            {
                entity.HasKey(e => new { e.ArticleId, e.ComponentId });

                entity.HasOne(e => e.Article)
                      .WithMany(a => a.ArticleComponents)
                      .HasForeignKey(e => e.ArticleId);
                
                entity.HasOne(e => e.Component)
                      .WithMany(c => c.ArticleComponents)
                      .HasForeignKey(e => e.ComponentId);

                entity.ToTable(name: "ArticleComponents");
            });

            #region Rename Identity Tables

            builder.Entity<IdentityUser>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(256);
                entity.ToTable(name: "Users");
            });

            builder.Entity<AppUser>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(256);
                entity.ToTable(name: "Users");
            });

            builder.Entity<IdentityRole>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(256);
                entity.ToTable(name: "Roles");
            });

            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable(name: "UserRoles");
            });

            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.Property(e => e.UserId).HasMaxLength(256);
                entity.Property(e => e.LoginProvider).HasMaxLength(256);
                entity.Property(e => e.ProviderKey).HasMaxLength(256);
                entity.ToTable(name: "UserLogins");
            });

            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.Property(e => e.UserId).HasMaxLength(256);
                entity.ToTable(name: "UserClaims");
            });

            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.Property(e => e.RoleId).HasMaxLength(256);
                entity.ToTable(name: "RoleClaims");
            });

            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.Property(e => e.UserId).HasMaxLength(256);
                entity.Property(e => e.LoginProvider).HasMaxLength(256);
                entity.Property(e => e.Name).HasMaxLength(256);
                entity.ToTable(name: "UserTokens");
            });

            #endregion
        }
    }
}
