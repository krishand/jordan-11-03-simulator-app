﻿using SLRS.Jordan.Storage.Repository.Interfaces;
using System.Threading.Tasks;

namespace SLRS.Jordan.Storage.UoW.Interfaces
{
    public interface IUoW
    {
        IArticleRepository ArticleRepository { get; }
        IComponentRepository ComponentRepository { get; }
        IMachineRepository MachineRepository { get; }
        ITrayRepository TrayRepository { get; }
        ITrayItemRepository TrayItemRepository { get; }
        ITrayMapRepository TrayMapRepository { get; }
        IRequestOrderRepository RequestOrderRepository { get; }
        ITrayItemItemCodeRepository TrayItemItemCodeRepository { get; }
        IItemCodeRepository ItemCodeRepository { get; }

        void Save();
        Task SaveAsync();
    }
}
