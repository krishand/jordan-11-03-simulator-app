﻿using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Repository;
using SLRS.Jordan.Storage.Repository.Interfaces;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System;
using System.Threading.Tasks;

namespace SLRS.Jordan.Storage.UoW
{
    public class UoW: IUoW, IDisposable
    {
        private bool _disposed = false;
        private JordanContext _context;
        private IArticleRepository _articleRepository;
        private IComponentRepository _componentRepository;
        private IMachineRepository _machineRepository;
        private ITrayRepository _trayRepository;
        private ITrayItemRepository _trayItemRepository;
        private ITrayMapRepository _trayMapRepository;
        private IRequestOrderRepository _requestOrderRepository;
        private ITrayItemItemCodeRepository _trayItemItemCodeRepository;
        private IItemCodeRepository _itemCodeRepository;
        //private IErrorLogRepository _logRepository;

        public UoW(JordanContext context)
        {
            _context = context ?? throw new ArgumentNullException("Context was not supplied"); ;
        }

        #region Repository

        public IArticleRepository ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                {
                    _articleRepository = new ArticleRepository(_context);
                }

                return _articleRepository;
            }
        }

        public IComponentRepository ComponentRepository
        {
            get
            {
                if (_componentRepository == null)
                {
                    _componentRepository = new ComponentRepository(_context);
                }

                return _componentRepository;
            }
        }

        public ITrayRepository TrayRepository
        {
            get
            {
                if (_trayRepository == null)
                {
                    _trayRepository = new TrayRepository(_context);
                }

                return _trayRepository;
            }
        }

        public ITrayItemRepository TrayItemRepository
        {
            get
            {
                if (_trayItemRepository == null)
                {
                    _trayItemRepository = new TrayItemRepository(_context);
                }

                return _trayItemRepository;
            }
        }

        public ITrayMapRepository TrayMapRepository
        {
            get
            {
                if (_trayMapRepository == null)
                {
                    _trayMapRepository = new TrayMapRepository(_context);
                }

                return _trayMapRepository;
            }
        }

        public IRequestOrderRepository RequestOrderRepository
        {
            get
            {
                if (_requestOrderRepository == null)
                {
                    _requestOrderRepository = new RequestOrderRepository(_context);
                }

                return _requestOrderRepository;
            }
        }

        public IMachineRepository MachineRepository
        {
            get
            {
                if (_machineRepository == null)
                {
                    _machineRepository = new MachineRepository(_context);
                }

                return _machineRepository;
            }
        }

        public ITrayItemItemCodeRepository TrayItemItemCodeRepository
        {
            get
            {
                if(_trayItemItemCodeRepository == null)
                {
                    _trayItemItemCodeRepository = new TrayItemItemCodeRepository(_context);
                }

                return _trayItemItemCodeRepository;
            }
        }

        public IItemCodeRepository ItemCodeRepository
        {
            get
            {
                if(_itemCodeRepository == null)
                {
                    _itemCodeRepository = new ItemCodeRepository(_context);
                }

                return _itemCodeRepository;
            }
        }

        //public IErrorLogRepository ErrorLogRepository
        //{
        //    get
        //    {
        //        if (_logRepository == null)
        //        {
        //            _logRepository = new ErrorLogRepository(_context);
        //        }

        //        return _logRepository;
        //    }
        //}

        #endregion

        #region Save

        public void Save()
        {
            _context.SaveChanges();
        }

        public Task SaveAsync()
        {
            return _context.SaveChangesAsync();
        }

        #endregion

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
