﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SLRS.Jordan.Storage.Context;

namespace SLRS.Jordan.Storage.Factories
{
    public class JordanContextFactory : IDesignTimeDbContextFactory<JordanContext>
    {
        public JordanContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<JordanContext>();
            //optionsBuilder.UseMySQL("server=35.203.147.28;database=JordanDb_Test;user=docqueue;password=DocQueue@123;SslMode=none");
            optionsBuilder.UseMySQL("server=localhost;database=JordanDb;user=JordanUser;password=root;SslMode=none");

            return new JordanContext(optionsBuilder.Options);
        }
    }
}
