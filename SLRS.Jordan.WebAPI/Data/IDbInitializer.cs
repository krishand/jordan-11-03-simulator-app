﻿namespace SLRS.Jordan.WebAPI.Data
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
