﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.Constants;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Models;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SLRS.Jordan.WebAPI.Data
{
    public class DbInitializer: IDbInitializer
    {
        private readonly IServiceProvider _serviceProvider;

        public DbInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        // This example just creates an Administrator role and one Admin users
        public async void Initialize()
        {
            using (var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                // Create database schema if none exists
                var context = serviceScope.ServiceProvider.GetService<JordanContext>();
                context.Database.EnsureCreated();

                // Create roles
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                var roles = new List<string>()
                {
                    Roles.Admin,
                    Roles.TrayUser
                };
                foreach(var role in roles)
                {
                    if (!(await roleManager.RoleExistsAsync(role)))
                    {
                        // Create the Administartor Role
                        await roleManager.CreateAsync(new IdentityRole(role));
                    }
                }

                // Create the default Admin account and apply the Administrator role
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<AppUser>>();
                string user = "slrs";
                string email = "sameera@slrobotics.com";
                string password = "Slrs@123";
                string name = "SLRobotics";
                var success = await userManager.CreateAsync(new AppUser { UserName = user, Email = email, EmailConfirmed = true, Name = name, IsActive = true }, password);
                if (success.Succeeded)
                {
                    await userManager.AddToRoleAsync(await userManager.FindByNameAsync(user), Roles.Admin);
                }

                var uow = serviceScope.ServiceProvider.GetService<IUoW>();

                // Create machines
                var machineManager = serviceScope.ServiceProvider.GetService<MachineManager>();
                var hasMachines = machineManager.GetAll().Any();
                if (!hasMachines)
                {
                    var machines = new List<Machine>()
                    {
                        new Machine { Name = "Machine 1" },
                        new Machine { Name = "Machine 2" }
                    };
                    await machineManager.AddRangeAsync(machines);
                }

                // Create trays
                var trayManager = serviceScope.ServiceProvider.GetService<TrayManager>();
                var hasTrays = trayManager.GetAll().Any();
                if (!hasTrays)
                {
                    for (int i = 1; i <= 120; i++)
                    {
                        var tray = new Tray
                        {
                            Number = i,
                        };
                        await trayManager.AddAsync(tray);
                    }
                }

                // Components
                var componentManager = serviceScope.ServiceProvider.GetService<ComponentManager>();
                var hasComponents = componentManager.GetAll().Any();
                var components = new List<Component>();
                if (!hasComponents)
                {
                    for (int i = 1; i <= 10; i++)
                    {
                        var component = new Component
                        {
                            Name = $"Component {i}",
                            IsActive = true
                        };
                        await componentManager.AddAsync(component);
                        components.Add(component);
                    }
                }

                // Create articles
                var articlesManager = serviceScope.ServiceProvider.GetService<ArticleManager>();
                var hasArticles = articlesManager.GetAll().Any();
                if (!hasArticles)
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        var article = new Article
                        {
                            Name = $"Article {i}",
                            IsActive = true
                        };
                        int componentIndex = i <= 10 ? (i - 1) : i - 11;
                        article.ArticleComponents = new List<ArticleComponent>
                        {
                            new ArticleComponent { Article = article, Component = components[componentIndex] }
                        };
                        await articlesManager.AddAsync(article);
                    }
                }
            }
        }
    }
}
