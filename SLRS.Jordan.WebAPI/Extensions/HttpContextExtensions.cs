﻿using System;
using System.Security.Claims;

namespace SLRS.Jordan.WebAPI.Extensions
{
    public static class HttpContextExtensions
    {
        public static string UserId(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}
