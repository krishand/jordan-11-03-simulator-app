﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.Models;
using SLRS.Jordan.Storage.UoW;
using SLRS.Jordan.Storage.UoW.Interfaces;
using SLRS.Jordan.WebAPI.Data;
using Swashbuckle.AspNetCore.Swagger;

namespace SLRS.Jordan.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<JordanContext>(opt =>
                opt.UseMySQL(Configuration.GetConnectionString("JordanDataConnection")));

            // Add Identity
            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<JordanContext>()
                .AddDefaultTokenProviders();

            // Remove Default Claims
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            // Add Jwt Authentication
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["SecurityToken:JwtIssuer"],
                        ValidAudience = Configuration["SecurityToken:JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityToken:JwtKey"])),
                        ClockSkew = TimeSpan.Zero, // remove delay of token when expire
                    };
                });

            services.AddScoped<IUoW, UoW>();
            services.AddScoped<MachineManager, MachineManager>();
            services.AddScoped<TrayManager, TrayManager>();
            services.AddScoped<TrayItemManager, TrayItemManager>();
            services.AddScoped<ArticleManager, ArticleManager>();
            services.AddScoped<ComponentManager, ComponentManager>();
            services.AddScoped<RequestOrderManager, RequestOrderManager>();

            // Add Database Initializer
            services.AddScoped<IDbInitializer, DbInitializer>();

            services.AddCors();

            services.AddAutoMapper();

            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver
                        = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling
                        = ReferenceLoopHandling.Ignore;
            });

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer",
                    new ApiKeyScheme()
                    {
                        In = "header",
                        Description = "Please insert JWT with Bearer into field",
                        Name = "Authorization",
                        Type = "apiKey"
                    });

                c.SwaggerDoc("v1", new Info { Title = "DocQueue API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, JordanContext dbContext, IDbInitializer dbInitializer, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            // Use CORS
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            // Use Authentication
            app.UseAuthentication();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DocQueue V1");
            });

            // Initialize database
            dbInitializer.Initialize();

            app.UseMvc();
        }
    }
}
