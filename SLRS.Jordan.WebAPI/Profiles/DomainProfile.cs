﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using SLRS.Jordan.Core.DTO.Articles;
using SLRS.Jordan.Core.DTO.Auth;
using SLRS.Jordan.Core.DTO.Requests;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.Storage.Models;

namespace SLRS.Jordan.WebAPI.Profiles
{
    public class DomainProfile: Profile
    {
        public DomainProfile()
        {
            CreateMap<AppUser, UserInfo>();
            CreateMap<RequestOrder, Order>()
                .ForMember(dest => dest.BatchNo, m => m.MapFrom(src => src.BatchNumber))
                .ForMember(dest => dest.Priority, m => m.MapFrom(src => src.OrderPriority))
                .ForMember(dest => dest.Status, m => m.MapFrom(src => src.Status))
                .ForMember(dest => dest.Articles, m => m.MapFrom(src => src.OrderItems)).ReverseMap();
            CreateMap<OrderItem, RequestedArticle>()
                .ForMember(dest => dest.NoOfArticlesNeeded, m => m.MapFrom(src => src.Quantity)).ReverseMap();
            CreateMap<Article, ArticleDTO>()
                .ForMember(dest => dest.Components, m => m.MapFrom(src => src.ArticleComponents)).ReverseMap();
            CreateMap<ArticleComponent, ArticleComponentDTO>().ReverseMap();
        }
    }
}
