﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.DTO;

namespace SLRS.Jordan.WebAPI.Controllers
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class TraysController : Controller
    {
        private readonly TrayManager _trayManager;
        private readonly TrayItemManager _trayItemManager;

        public TraysController(TrayManager trayManager, TrayItemManager trayItemManager)
        {
            _trayManager = trayManager;
            this._trayItemManager = trayItemManager;
        }

        public IActionResult Get()
        {
            var result = _trayManager.GetAll();
            var x = new ObjectResult(result);
            return x;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _trayManager.GetById(id);
            if (result == null)
            {
                return NotFound();
            }
            return new ObjectResult(result);
        }

        [HttpPost("create/trayitem")]
        public IActionResult CreateTrayItem([FromBody] TrayItemWithItemCode trayItemWithCode)
        {
            return new ObjectResult(_trayItemManager.CreateTrayItem(trayItemWithCode));
        }

        [HttpPost("issue/trayitem")]
        public IActionResult IssueTrayITem([FromBody] List<TrayItemWithTags> trayItemWithTags)
        {
            return new ObjectResult(_trayItemManager.IssueTrayItem(trayItemWithTags));
        }

    }
}