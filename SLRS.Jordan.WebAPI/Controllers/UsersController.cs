﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SLRS.Jordan.Core.DTO.Auth;
using SLRS.Jordan.Storage.Models;

namespace SLRS.Jordan.WebAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;

        public UsersController(IMapper mapper, UserManager<AppUser> userManager)
        {
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var entities = _userManager.Users.ToList();

            var users = _mapper.Map<IEnumerable<UserInfo>>(entities);

            foreach(var user in users)
            {
                user.Role = (await _userManager.GetRolesAsync(new AppUser { Id = user.Id })).FirstOrDefault();
            }

            return new ObjectResult(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var entity = await _userManager.FindByIdAsync(id);

            if (entity == null) return NotFound();

            var user = _mapper.Map<UserInfo>(entity);

            user.Role = (await _userManager.GetRolesAsync(entity)).FirstOrDefault();

            return new ObjectResult(user);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]UserInfo user)
        {
            if (id != user.Id) return BadRequest("Id's are not matching");

            var entity = _userManager.Users.FirstOrDefault(u => u.Id == user.Id);

            if (entity == null) return NotFound();

            if (ModelState.IsValid)
            {
                entity.UserName = user.UserName;
                entity.Email = user.Email;
                entity.PhoneNumber = user.PhoneNumber;
                await _userManager.UpdateAsync(entity);

                var role = (await _userManager.GetRolesAsync(entity)).FirstOrDefault();
                if (!role.Equals(user.Id))
                {
                    await _userManager.RemoveFromRoleAsync(entity, role);
                    await _userManager.AddToRoleAsync(entity, user.Role);
                }
                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var item = await _userManager.FindByIdAsync(id);
            if (item != null)
            {
                item.IsActive = false;
                await _userManager.UpdateAsync(item);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
