﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SLRS.Jordan.Core.DTO.Auth;
using SLRS.Jordan.Storage.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SLRS.Jordan.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;


        public AccountController(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration
            )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpGet]
        [Authorize]
        public IActionResult UserClaims()
        {
            var claims = HttpContext.User.Claims;
            List<object> result = new List<object>();
            foreach(var claim in claims)
            {
                result.Add(new { Type = claim.Type, Value = claim.Value });
            }
            return new ObjectResult(result);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Roles()
        {
            var roles = _roleManager.Roles.Select(r => new Role
            {
                Id = r.Id,
                Name = r.Name
            });
            return new ObjectResult(roles);
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);

            if (result.Succeeded)
            {
                var user = _userManager.Users.SingleOrDefault(r => r.UserName == model.UserName);

                return new ObjectResult(GenerateJwtToken(model.UserName, user));
            }

            return BadRequest("INVALID_LOGIN_ATTEMPT");
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]Register model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // TODO: Add role
            var user = new AppUser
            {
                UserName = model.UserName,
                Email = model.Email,
                Name = model.Name
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, model.Role);

                await _signInManager.SignInAsync(user, false);
                return new ObjectResult(GenerateJwtToken(model.UserName, user));
            }

            return StatusCode((int)HttpStatusCode.InternalServerError, "UNKNOWN_ERROR");
        }

        private async Task<object> GenerateJwtToken(string userName, AppUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            // Get roles
            var roles = await _userManager.GetRolesAsync(user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityToken:JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["SecurityToken:JwtExpireDays"]));

            // TODO: returning value should contains expiration time in seconds
            var token = new JwtSecurityToken(
                _configuration["SecurityToken:JwtIssuer"],
                _configuration["SecurityToken:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}