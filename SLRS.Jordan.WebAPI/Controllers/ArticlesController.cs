﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.DTO.Articles;
using SLRS.Jordan.Core.Models.Articles;

namespace SLRS.Jordan.WebAPI.Controllers
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ArticlesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ArticleManager _articleManager;

        public ArticlesController(IMapper mapper, ArticleManager articleManager)
        {
            _mapper = mapper;
            _articleManager = articleManager;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var entities = _articleManager.GetAll();
            var articles = _mapper.Map<IEnumerable<ArticleDTO>>(entities);
            return new ObjectResult(articles);
        }

        [HttpGet("{id}", Name = "GetArticle")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _articleManager.GetById(id);

            if (entity == null) return NotFound();

            var article = _mapper.Map<ArticleDTO>(entity);
            return new ObjectResult(article);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ArticleDTO article)
        {
            if (ModelState.IsValid)
            {
                var entity = _mapper.Map<Article>(article);
                await _articleManager.AddAsync(entity);
                article.Id = entity.Id;
                article = _mapper.Map<ArticleDTO>(entity);
                return CreatedAtRoute("GetArticle", new { id = article.Id }, article);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody]ArticleDTO article)
        {
            if (id != article.Id) return BadRequest("Invalid Request: Id's are not matching");
            if (ModelState.IsValid)
            {
                var entity = await _articleManager.GetById(id);
                if (entity == null) return NotFound();

                entity.Name = article.Name;
                entity.ArticleComponents = _mapper.Map<ICollection<ArticleComponent>>(article.Components);
                _articleManager.Update(entity);

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _articleManager.GetById(id);
            if (item != null)
            {
                item.IsActive = false;
                _articleManager.Update(item);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{articleid}/trays")]
        public IActionResult GetAllTraysFromArticle(int articleid)
        {
            var x = new ObjectResult(_articleManager.GetAllTraysFromArticle(articleid));
            return x;
        }

        [HttpGet("{articleid}/tray")]
        public IActionResult GetTrayFromArticle(int articleid)
        {
            var x = new ObjectResult(_articleManager.GetTrayFromArticle(articleid));
            return x;
        }

        [HttpPost("tags/count/")]
        public IActionResult GetArticleWithTags([FromBody]List<string> tags)
        {
            return new ObjectResult(_articleManager.GetArticleWithTags(tags));
        }

        [HttpGet("total")]
        public IActionResult GetArticlesWithWeight()
        {
            return new ObjectResult(_articleManager.GetArticleWithWeight());
        }
    }
}
