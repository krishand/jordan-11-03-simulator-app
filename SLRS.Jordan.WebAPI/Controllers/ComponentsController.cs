﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.Models.Articles;

namespace SLRS.Jordan.WebAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class ComponentsController : Controller
    {
        private readonly ComponentManager _componentManager;

        public ComponentsController(ComponentManager componentManager)
        {
            _componentManager = componentManager;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return new ObjectResult(_componentManager.GetAll());
        }

        [HttpGet("{id}", Name = "GetComponent")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _componentManager.GetById(id);

            if (entity == null) return NotFound();

            return new ObjectResult(entity);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]Component component)
        {
            if (ModelState.IsValid)
            {
                await _componentManager.AddAsync(component);
                return CreatedAtRoute("GetComponent", new { id = component.Id }, component);
            }
            else 
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody]Component component)
        {
            if (id != component.Id) return BadRequest("Invalid Request: Id's are not matching");

            if (ModelState.IsValid)
            {
                var entity = await _componentManager.GetById(id);

                if (entity == null) return NotFound();

                entity.Name = component.Name;
                _componentManager.Update(entity);

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await _componentManager.GetById(id);

            if (entity == null) return NotFound();

            entity.IsActive = false;
            _componentManager.Update(entity);
            return Ok();
        }
    }
}
