using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.DTO.Requests;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WebAPI.Extensions;

namespace SLRS.Jordan.WebAPI.Controllers
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly RequestOrderManager _requestOrderManager;

        public OrdersController(IMapper mapper, RequestOrderManager requestOrderManager)
        {
            _mapper = mapper;
            _requestOrderManager = requestOrderManager;
        }

        [HttpGet("{id}", Name = "GetOrder")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _requestOrderManager.Get(id);

            if (entity == null) return NotFound();

            var order = _mapper.Map<Order>(entity);
            return new ObjectResult(order);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var entities = _requestOrderManager.GetAll();
            var orders = _mapper.Map<IEnumerable<Order>>(entities);
            return new ObjectResult(orders);
        }

        [HttpGet("user")]
        public IActionResult GetUserOrders()
        {
            var userId = User.UserId();
            var entities = _requestOrderManager.GetOrdersByUser(userId);
            var orders = _mapper.Map<IEnumerable<Order>>(entities);
            return new ObjectResult(orders);
        }

        [HttpGet("close/{orderId}")]
        public IActionResult CloseRequestOrder(int orderId)
        {
            return new ObjectResult(_requestOrderManager.CloseRequestOrder(orderId));
        }

        [HttpPost]
        public IActionResult Create([FromBody]Order request)
        {
            var userId = User.UserId();
            if (ModelState.IsValid)
            {
                var entity = _mapper.Map<RequestOrder>(request);
                entity.UserId = userId;
                _requestOrderManager.Add(entity);
                var order = _mapper.Map<Order>(entity);
                return CreatedAtRoute("GetOrder", new { id = order.Id }, order);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet("pending")]
        public IActionResult PendingOrders()
        {
            return new ObjectResult(_requestOrderManager.GetPendingOrders());
        }

        //sort pending order group by machine
        [HttpGet("{requestOrderId}/machine")]
        public IActionResult GetRequestOrderGroupByMachine(int requestOrderId)
        {
            return new ObjectResult(_requestOrderManager.GetRequestOrderGroupByMachine(requestOrderId));
        }
    }
}