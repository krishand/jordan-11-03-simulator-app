﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SLRS.Jordan.Business.Managers
{
    public class TrayManager
    {
        private IUoW _uow;

        public TrayManager(IUoW uow)
        {
            _uow = uow;
        }

        public async Task AddAsync(Tray tray)
        {
            await _uow.TrayRepository.AddAsync(tray);
            _uow.Save();
        }

        public async Task AddRangeAsync(IEnumerable<Tray> trays)
        {
            await _uow.TrayRepository.AddRangeAsync(trays);
            _uow.Save();
        }

        public IQueryable<Tray> GetAll()
        {
            return _uow.TrayRepository.GetAll();
        }

        public Task<Tray> GetById(int id)
        {
            return _uow.TrayRepository.GetById(id);
        }

    }
}
