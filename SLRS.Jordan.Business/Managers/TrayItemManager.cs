﻿using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SLRS.Jordan.Business.Managers
{
    public class TrayItemManager
    {
        private IUoW _uow;

        public TrayItemManager(IUoW uow)
        {
            _uow = uow;
        }

        public async Task AddAsync(TrayItem trayItem)
        {
            await _uow.TrayItemRepository.AddAsync(trayItem);
            _uow.Save();
        }

        public object CreateTrayItem(TrayItemWithItemCode trayItemWithCode)
        {
            var trayItem = trayItemWithCode.TrayItem;
            var itemCodes = trayItemWithCode.ItemCodes;

            //TODO Fix Transaction Bug 
            //using (var transaction  = new TransactionScope())
            //{

            //create tray item
            _uow.TrayItemRepository.Add(trayItem);
            _uow.Save();

            //create item codes
            _uow.ItemCodeRepository.AddRange(itemCodes);
            _uow.Save();

            //add to trayitem item code
            var newTrayItemItemCodes = new List<TrayItemItemCode>();
            foreach (var item in itemCodes)
            {
                newTrayItemItemCodes.Add(new TrayItemItemCode()
                {
                    ItemCodeId = item.Id,
                    TrayItemId = trayItem.Id,
                });
            }
            _uow.TrayItemItemCodeRepository.AddRange(newTrayItemItemCodes);

            _uow.Save();

            //transaction.Complete();
            //}

            return trayItem.Id;

        }

        public object IssueTrayItem(List<TrayItemWithTags> trayItemWithTags)
        {
            foreach (var item in trayItemWithTags)
            {
                //insert tray item
                _uow.TrayItemRepository.Add(item.TrayItem);
                _uow.Save();

                //inster tages
                if (item.ArticleCount != null)
                {
                    foreach (var tag in item.ArticleCount.Tag)
                    {
                        var itemCode = _uow.ItemCodeRepository.Find(t => t.RfidCode == tag).FirstOrDefault();
                        if (itemCode == null)
                        {
                            throw new Exception("Unknown Tag Found");
                        }
                        _uow.TrayItemItemCodeRepository.Add(new TrayItemItemCode()
                        {
                            ItemCodeId = itemCode.Id,
                            TrayItemId = item.TrayItem.Id
                        });

                        _uow.Save();
                    };
                }
            }

            return 0;
        }
    }
}
