﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Storage.UoW.Interfaces;

namespace SLRS.Jordan.Business.Managers
{
    public class ComponentManager
    {
        private IUoW _uow;

        public ComponentManager(IUoW uow)
        {
            _uow = uow;
        }

        public async Task AddAsync(Component component)
        {
            component.IsActive = true;
            await _uow.ComponentRepository.AddAsync(component);
            _uow.Save();
        }

        public void Update(Component component)
        {
            _uow.ComponentRepository.Update(component);
            _uow.Save();
        }

        public Task<Component> GetById(int id)
        {
            return _uow.ComponentRepository.GetById(id);
        }

        public IQueryable<Component> GetAll()
        {
            return _uow.ComponentRepository.GetAll();
        }
    }
}
