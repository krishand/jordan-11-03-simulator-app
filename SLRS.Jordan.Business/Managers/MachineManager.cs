﻿using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SLRS.Jordan.Business.Managers
{
    public class MachineManager
    {
        private IUoW _uow;

        public MachineManager(IUoW uow)
        {
            _uow = uow;
        }

        public async Task AddAsync(Machine machine)
        {
            await _uow.MachineRepository.AddAsync(machine);
            _uow.Save();
        }

        public async Task AddRangeAsync(IEnumerable<Machine> machines)
        {
            await _uow.MachineRepository.AddRangeAsync(machines);
            _uow.Save();
        }

        public IQueryable<Machine> GetAll()
        {
            return _uow.MachineRepository.GetAll();
        }
    }
}
