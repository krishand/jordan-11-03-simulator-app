﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.DTO.Requests;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.Storage.UoW.Interfaces;

namespace SLRS.Jordan.Business.Managers
{
    
    public class RequestOrderManager
    {
        private IUoW _uow;

        public RequestOrderManager(IUoW uow)
        {
            _uow = uow;
        }

        public void Add(RequestOrder entity)
        {
            _uow.RequestOrderRepository.Add(entity);
            _uow.Save();
        }

        public Task<RequestOrder> Get(int id)
        {
            return _uow.RequestOrderRepository.GetById(id);
        }

        public IQueryable<RequestOrder> GetAll()
        {
            return _uow.RequestOrderRepository.GetAll().Include(o => o.OrderItems);
        }

        public IQueryable<RequestOrder> GetOrdersByUser(string userId)
        {
            return _uow.RequestOrderRepository.Find(t => t.UserId == userId).Include(t => t.OrderItems);
        }

        public object CloseRequestOrder(int orderId)
        {
            var x = _uow.RequestOrderRepository.Find(t => t.Id == orderId).First();
            x.Status = OrderStatus.Closed;

            _uow.Save();

            return true;
        }

        // New
        public object GetPendingOrders()
        {
            var y = _uow.RequestOrderRepository.GetAll();

            var x = _uow.RequestOrderRepository
                .Find(t => t.Status == OrderStatus.Pending)

                .Include(t => t.OrderItems)
                .ThenInclude(t => t.Article)

                .Include(t => t.Component).ToList();

            return x;
        }

        public object GetUserOrders(string userId)
        {
            var x = _uow.RequestOrderRepository.Find(t => t.UserId == userId)

                .Include(t => t.OrderItems)
                .ThenInclude(t => t.Article)

                .Include(t => t.Component).ToList();

            return x;
        }

        public object GetRequestOrderGroupByMachine(int requestOrderId)
        {
            return _uow.RequestOrderRepository.GetRequestOrderGroupByMachine(requestOrderId);
        }
    }
}
