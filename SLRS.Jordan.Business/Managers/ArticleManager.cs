﻿using Microsoft.EntityFrameworkCore;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SLRS.Jordan.Business.Managers
{
    public class ArticleManager
    {
        private IUoW _uow;

        public ArticleManager(IUoW uow)
        {
            _uow = uow;
        }

        public async Task AddAsync(Article article)
        {
            article.IsActive = true;
            await _uow.ArticleRepository.AddAsync(article);
            _uow.Save();
        }

        public void Update(Article article)
        {
            _uow.ArticleRepository.Update(article);
            _uow.Save();
        }

        public IQueryable<Article> GetAll()
        {
            return _uow.ArticleRepository.GetAll().Include(a => a.ArticleComponents);
        }

        public Task<Article> GetById(int id)
        {
            return _uow.ArticleRepository.GetById(id);
        }

        public object GetArticleWithTags(List<string> tags)
        {
            var articleCount = new List<ArticleCount>();

            //get article
            foreach (var tag in tags)
            {
                var article = _uow.ItemCodeRepository.Find(t => t.RfidCode == tag).Select(t => t.Article).FirstOrDefault();
                if (article == null)
                {
                    throw new Exception("RFID Not found");
                }

                // added article count
                var existingArticle = articleCount.Where(t => t.Article == article).FirstOrDefault();
                if (existingArticle == null)
                {
                    var x = new ArticleCount()
                    {
                        Article = article,
                        Count = 1,
                        Tag = new List<string>(),
                    };
                    x.Tag.Add(tag);

                    articleCount.Add(x);
                }
                else
                {
                    existingArticle.Count += 1;
                    existingArticle.Tag.Add(tag);
                }
            }

            return articleCount;
        }

        public object GetArticleWithWeight()
        {
            var articleWithWeight = _uow.TrayItemRepository.GetAll()
               .Include(t => t.Article)
               .GroupBy(t => t.Article)
               .Select(t => new ArticleWithTotal()
               {
                   Article = t.Key,
                   TotalWeight = t.Sum(t1 => t1.Weight),
                   TotalQuantity = t.Sum(t1 => t1.Quantity),
               }).ToList();


            return articleWithWeight;
        }

        public object GetTrayFromArticle(int articleid)
        {
            //get the last tray it inserted
            var tray = _uow.TrayItemRepository.Find(t => t.ArticleId == articleid).Select(t => t.Tray).LastOrDefault();

            //if not get from tray map
            if (tray == null)
            {
                tray = _uow.TrayMapRepository.Find(t => t.ArticleId == articleid).Select(t => t.Tray).LastOrDefault();
            }

            return tray;
        }

        public object GetAllTraysFromArticle(int articleid)
        {
            var article = _uow.ArticleRepository.Find(t1 => t1.Id == articleid).First();
            var tray = _uow.TrayItemRepository.Find(t => t.ArticleId == articleid).GroupBy(t => t.Tray)
                .Select(t => new TrayWithTotalArticleQuantity() {
                    Article = article,
                    TotalArticleQuantity = t.Sum(t1 => t1.Quantity),
                    Tray = t.Key
                });

            return tray;
        }
    }
}
