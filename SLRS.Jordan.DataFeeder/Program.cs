﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SLRS.Jordan.Business.Managers;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Storage.Context;
using SLRS.Jordan.Storage.UoW;
using SLRS.Jordan.Storage.UoW.Interfaces;
using System;
using System.Collections.Generic;

namespace SLRS.Jordan.DataFeeder
{
    public class Program
    {
        private static IUoW _uow;

        static void Main(string[] args)
        {
            try
            {
                Initialize();

                FeedData();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Finished ...");
                Console.Read();
            }
        }

        static void Initialize()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var connectionString = config.GetConnectionString("JordanDataConnection");

            Console.WriteLine(connectionString);

            // Initialize context
            var optionsBuilder = new DbContextOptionsBuilder<JordanContext>();
            optionsBuilder.UseMySQL(connectionString);
            var context = new JordanContext(optionsBuilder.Options);
            context.Database.EnsureCreated();

            _uow = new UoW(context);

            
        }

        static async void FeedData()
        {
            // Fill trays
            var trayManager = new TrayManager(_uow);
            var any = await trayManager.GetAll().AnyAsync();
            if (!any)
            {
                var trays = new List<Tray>();
                for (int i = 1; i <= 120; i++)
                {
                    var tray = new Tray
                    {
                        Number = i,
                        MachineId = (i <= 60 ? 1 : 2)
                    };
                    trays.Add(tray);
                }
                await trayManager.AddRangeAsync(trays);
            }
        }


    }
}
