﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using SLRS.Jordan.WindowsApp.Network;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SLRS.Jordan.WindowsApp.ViewModel
{
    class RequestViewModel : ViewModelBase
    {

        private bool _isTrayOpened;
        public bool IsTrayOpended
        {
            set
            {
                _isTrayOpened = value;
                RaisePropertyChanged(nameof(IsTrayOpended));
            }
            get
            {
                return _isTrayOpened;
            }
        }

        private List<Tray> _opendedTrays = new List<Tray>();
        public List<Tray> OpenedTrays
        {
            set
            {
                _opendedTrays = value;
                RaisePropertyChanged(nameof(OpenedTrays));
            }
            get
            {
                return _opendedTrays;
            }
        }

        private ObservableCollection<RequestOrderWithTotalQuantity> _requestOrderList;
        public ObservableCollection<RequestOrderWithTotalQuantity> RequestOrderList
        {
            get
            {
                return _requestOrderList;
            }
            set
            {
                _requestOrderList = value;
                RaisePropertyChanged(nameof(RequestOrderList));
            }
        }

        private Article _selectedArticle;
        public Article SelectedArticle
        {
            get
            {
                return _selectedArticle;
            }
            set
            {
                _selectedArticle = value;
                RaisePropertyChanged(nameof(SelectedArticle));
            }
        }

        private RequestOrderWithTotalQuantity _selectedRequestOrder;
        public RequestOrderWithTotalQuantity SelectedRequestOrder {
            get
            {
                return _selectedRequestOrder;
            }
            set
            {
                _selectedRequestOrder = value;
                RaisePropertyChanged(nameof(SelectedRequestOrder));
            }
        }

        private RequestOrderWithTotalQuantity _selectedRequestOrderInQueue;
        public RequestOrderWithTotalQuantity SelectedRequestOrderInQueue
        {
            get
            {
                return _selectedRequestOrderInQueue;
            }
            set
            {
                _selectedRequestOrderInQueue = value;
                RaisePropertyChanged(nameof(SelectedRequestOrderInQueue));
            }
        }

        private int _articleQty;
        public int ArticleQty
        {
            get
            {
                return _articleQty;
            }
            set
            {
                _articleQty = value;
                RaisePropertyChanged(nameof(ArticleQty));
            }
        }

        private ObservableCollection<TrayItemsGroupByTray> _selectedTrays;
        public ObservableCollection<TrayItemsGroupByTray> SelectedTrays
        {
            get
            {
                return _selectedTrays;
            }
            set
            {
                _selectedTrays = value;
                RaisePropertyChanged(nameof(SelectedTrays));
            }
        }

        public ObservableCollection<TrayItemsGroupByTray> _requestedTrays;
        public ObservableCollection<TrayItemsGroupByTray> RequestedTrays
        {
            get
            {
                return _requestedTrays;
            }
            set
            {
                _requestedTrays = value;
                RaisePropertyChanged(nameof(RequestedTrays));
            }
        }

        private string _articleText;

        public string ArticleText
        {
            get
            {
                return _articleText;
            }
            set
            {
                _articleText = value;
                RaisePropertyChanged(nameof(ArticleText));
            }
        }

        private readonly MainWindowViewModel mainViewModel;

        #region Events
        public RelayCommand RequestArticleCommand { get; set; }
        public RelayCommand RequestCommand { get; set; }
        public RelayCommand<RequestOrderWithTotalQuantity> ArticleChangeCommand { get; set; }
        public RelayCommand<RequestOrderWithTotalQuantity> RequestOrdersItemCommand { get; set; }
        public RelayCommand<TrayItemsGroupByTray> TrayCloseCommand { get; set; }
        public RelayCommand<TrayItemsGroupByTray> TrayOpenCommand { get; set; }
        public RelayCommand<ListBox> ArticleSearchCommand { get; set; }
        public RelayCommand RequestOrderCloseCommand { get; set; }
        #endregion

        public RequestViewModel(MainWindowViewModel mainViewModel)
        {
            RequestArticleCommand = new RelayCommand(RequestArticleEvent, RequestArticleCanExecute);
            RequestCommand = new RelayCommand(RequestEvent, GetPendingOrdersCanExecute);
            ArticleChangeCommand = new RelayCommand<RequestOrderWithTotalQuantity>(ArticleChangeEvent);
            RequestOrdersItemCommand = new RelayCommand<RequestOrderWithTotalQuantity>(RequestOrderItemEvent, RequestOrderItemCanExecute);
            ArticleSearchCommand = new RelayCommand<ListBox>(ArticleSearchEvent);
            TrayCloseCommand = new RelayCommand<TrayItemsGroupByTray>(TrayCloseEvent, TrayCloseCanExecute);
            RequestOrderCloseCommand = new RelayCommand(RequestOrderCloseEvent, RequestOrderCloseCanExecute);
            TrayOpenCommand = new RelayCommand<TrayItemsGroupByTray>(TrayOpenEvent, TrayOpenCanExecute);
            this.mainViewModel = mainViewModel;
        }

        public void TrayOpenEvent(TrayItemsGroupByTray trayItem)
        {
            OpenedTrays.Add(trayItem.Tray);

            NetworkApp.str = trayItem.Tray.Id.ToString();
            mainViewModel.TrayStatus = NetworkApp.rackside;

            IsTrayOpended = true;
        }
        public bool TrayOpenCanExecute(TrayItemsGroupByTray trayItem)
        {
            if(OpenedTrays.Count() == 0)
            {
                return !IsTrayOpended;
            }
            else
            {
                var openedTray = OpenedTrays[0];

                if (OpenedTrays.Count > 1)
                {
                    return false;
                }

                if(openedTray.Id > 60 && trayItem.Tray.Id < 60)
                {
                    return true;
                }
                else if(openedTray.Id < 60 && trayItem.Tray.Id > 60)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            //return !IsTrayOpended;
        }
        public async void TrayCloseEvent(TrayItemsGroupByTray trayItem)
        {
            OpenedTrays.Remove( OpenedTrays.Where(t=>t.Id == trayItem.Tray.Id).FirstOrDefault());

            IsTrayOpended = false;

            var newClosingTray = new ClosingTrayItem()
            {
                RequestOrder = SelectedRequestOrderInQueue,
                TrayItem = trayItem,
            };

            NetworkApp.str = "-" + trayItem.Tray.Id.ToString();
            mainViewModel.TrayStatus = NetworkApp.rackside;

            var restOfQuantity = await mainViewModel.TrayService.ClosePendingTray(newClosingTray);
            SelectedRequestOrderInQueue.Quantity = restOfQuantity;
            RequestOrderList = new ObservableCollection<RequestOrderWithTotalQuantity>(RequestOrderList);

        }

        public bool TrayCloseCanExecute(TrayItemsGroupByTray arg)
        {
            foreach(var openedTray in OpenedTrays)
            {
                var thisTrayExist = OpenedTrays.Where(t => t.Id == arg.Tray.Id);

                if(thisTrayExist.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public bool RequestOrderCloseCanExecute()
        {
            if(SelectedRequestOrderInQueue != null && !IsTrayOpended)
            {
                return true;
            }

            return false;
        }

        public async void RequestOrderCloseEvent()
        {
            var requestOrder = SelectedRequestOrderInQueue;
            var result = await mainViewModel.TrayService.CloseRequestOrder(requestOrder);
            if (result > 0)
            {
                RequestOrderList.Remove(requestOrder);
            }

            //update ui
            mainViewModel.LoadedItems = await mainViewModel.TrayService.GetAllTrayItems();
            var restOfSelectedArticles = RequestOrderList.Where(t => t.ArticleId == SelectedRequestOrderInQueue.ArticleId);
            foreach (var item in restOfSelectedArticles)
            {
                item.TotalQuantity = (item.TotalQuantity - SelectedRequestOrderInQueue.Quantity);
            }

            RequestOrderList = new ObservableCollection<RequestOrderWithTotalQuantity>(RequestOrderList);

            SelectedRequestOrderInQueue = null;
            RequestedTrays = null;
        }


        private void ArticleSearchEvent(ListBox uiArticleListBox)
        {
            var searchString = ArticleText;

            if (mainViewModel.Articles != null)
            {
                uiArticleListBox.ItemsSource = mainViewModel.LoadedItems.Where(t => t.Article.Name.ToLower().Contains(searchString.ToLower()));
            }

            if (ArticleText == "")
            {
                SelectedTrays = null;
            }
        }

        private async void ArticleChangeEvent(RequestOrderWithTotalQuantity requestOrder)
        {

            SelectedArticle = requestOrder.Article;
            SelectedRequestOrder = requestOrder;

            ArticleText = SelectedArticle.Name;
            mainViewModel.StatusWait("Getting tray number");
            SelectedTrays = await mainViewModel.ArticleService.GetTrayListFromArticle(SelectedArticle);
            mainViewModel.StatusReady();
        }

        private async void RequestOrderItemEvent(RequestOrderWithTotalQuantity requestOrder)
        {
            SelectedRequestOrderInQueue = requestOrder;
            var article = mainViewModel.Articles.Where(t => t.Id == requestOrder.ArticleId).First();
            RequestedTrays = await mainViewModel.ArticleService.GetTrayListFromArticle(article);


        }
        private bool RequestOrderItemCanExecute(RequestOrderWithTotalQuantity requestOrder)
        {
            return !IsTrayOpended;
        }


        private async void RequestEvent()
        {
            mainViewModel.LoadedItems = await mainViewModel.TrayService.GetAllTrayItems();

            if (RequestOrderList == null)
            {
                mainViewModel.StatusWait("Getting pending orders");
                RequestOrderList = await mainViewModel.OrdersService.GetPendingOrders();
                mainViewModel.StatusReady();
            }
        }

        private bool GetPendingOrdersCanExecute()
        {
            var canExecute = true;

            //if (RequestOrderList != null)
            //{
            //    foreach (var item in RequestOrderList)
            //    {
            //        //if (item.ButtonText == "Close")
            //        //{
            //        //    canExecute = false;
            //        //}
            //    }
            //}

            return canExecute;
        }

        public async void RequestArticleEvent()
        {
            mainViewModel.StatusWait("Confirming");

            //validate qty
            var articleAvailableQty = await mainViewModel.TrayService.TotalAvailableArticles(SelectedArticle);
            if (articleAvailableQty.AvailableQty < ArticleQty)
            {
                MessageBox.Show(string.Format("Total AVAILABLE quantity of the request item is: {0} But you requested: {1}",
                    articleAvailableQty.AvailableQty, ArticleQty), "Quantity Exceeds");
                mainViewModel.StatusReady();
                return;
            }

            var status = await mainViewModel.OrdersService.NewRequestOrder(SelectedArticle, ArticleQty);

            if (status > 0)
            {
                var newPendingOrder = new RequestOrderWithTotalQuantity()
                {
                    ArticleId = SelectedArticle.Id,
                    Article = SelectedArticle,
                    Id = status,
                    Quantity = ArticleQty,
                    TotalQuantity = articleAvailableQty.AvailableQtyWithoutRO,
                    Status = OrderStatus.Pending,
                };

                //var isAnyTrayOpen = RequestOrderList.Where(t => t.ButtonText == "Close");

                //if (isAnyTrayOpen.Count() > 0)
                //{
                //    newPendingOrder.IsDisabled = true;
                //}

                RequestOrderList.Add(newPendingOrder);
            }

            ArticleQty = 0;
            RequestArticleCommand.RaiseCanExecuteChanged();
            mainViewModel.StatusReady();
        }

        public bool RequestArticleCanExecute()
        {
            if (ArticleQty > 0 && SelectedArticle != null && RequestOrderList != null)
            {
                return true;
            }
            return false;
        }

        //Change request order total quantity
        public void ChangeRequestOrderQty(Article article, int qantity)
        {
            if(RequestOrderList != null)
            {
                foreach (var order in RequestOrderList)
                {
                    if (order.ArticleId == article.Id)
                    {
                        order.TotalQuantity += qantity;
                    }
                }

                //To Notify Property change
                var newOrderList = new ObservableCollection<RequestOrderWithTotalQuantity>(RequestOrderList);
                RequestOrderList = newOrderList;
            }
            
        }

    }
}
