﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using SLRS.Jordan.WindowsApp.Network;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SLRS.Jordan.WindowsApp.ViewModel
{
    class LoadingViewModel : ViewModelBase
    {

        private readonly MainWindowViewModel mainViewModel;


        private Article _selectedArticle { get; set; }
        public Article SelectedArticle
        {
            get
            {
                return _selectedArticle;
            }
            set
            {
                _selectedArticle = value;
                RaisePropertyChanged(nameof(SelectedArticle));
            }
        }

        private Tray _selectedTray;
        public Tray SelectedTray
        {
            get
            {
                return _selectedTray;
            }
            set
            {
                _selectedTray = value;
                RaisePropertyChanged(nameof(SelectedTray));
            }
        }

        private ObservableCollection<TrayItem> _selectedTrayItems;
        public ObservableCollection<TrayItem> SelectedTrayItems
        {
            set
            {
                if (value != null)
                {
                    _selectedTrayItems = value;
                    RaisePropertyChanged(nameof(SelectedTrayItems));
                }
            }
            get
            {
                return _selectedTrayItems;
            }
        }

        private int _articleQty;
        public int ArticleQty
        {
            set
            {
                _articleQty = value;
                RaisePropertyChanged(nameof(ArticleQty));
            }
            get
            {
                return _articleQty;
            }
        }


        private bool _trayOpen { get; set; }
        public bool TrayOpened
        {
            get
            {
                return _trayOpen;
            }
            set
            {
                _trayOpen = value;
                RaisePropertyChanged(nameof(TrayOpened));
            }
        }

        private string _articleText;
        public string ArticleText
        {
            get
            {
                return _articleText;
            }
            set
            {
                _articleText = value;
                RaisePropertyChanged(nameof(ArticleText));
            }
        }


        #region Events
        public RelayCommand<Article> ArticleChangeCommand { get; set; }
        public RelayCommand CallTrayOpenCommand { get; set; }
        public RelayCommand AddStockCommand { get; set; }
        public RelayCommand ChangeTrayNumberCommand { get; set; }
        public RelayCommand LoadingCommand { get; set; }
        public RelayCommand<ListBox> ArticleSearchCommand { get; set; }

        #endregion

        public LoadingViewModel(MainWindowViewModel mainViewModel)
        {
            ArticleChangeCommand = new RelayCommand<Article>(ArticleChangeEvent, ArticleChangeCanExecute);
            CallTrayOpenCommand = new RelayCommand(CallTrayOpenEvent, CallTrayOpenEventCanExecute);
            AddStockCommand = new RelayCommand(AddStockEvent, AddStockEventCanExecute);
            ChangeTrayNumberCommand = new RelayCommand(ChangeTrayNumberEvent, ChangeTrayNumberCanExecute);
            LoadingCommand = new RelayCommand(LoadingEvent, LoadingEventCanExecute);
            ArticleSearchCommand = new RelayCommand<ListBox>(ArticleSearchEvent);

            this.mainViewModel = mainViewModel;
        }

        private void ArticleSearchEvent(ListBox uiArticleListBox)
        {
            if (mainViewModel.Articles != null)
            {
                var searchString = ArticleText;
                uiArticleListBox.ItemsSource = mainViewModel.Articles.Where(t => t.Name.ToLower().Contains(searchString.ToLower()));
            }
        }


        private async void ChangeTrayNumberEvent()
        {
            if (SelectedTray != null)
            {
                mainViewModel.StatusWait("Getting tray items");
                SelectedTrayItems = await mainViewModel.TrayService.GetTrayItems(SelectedTray);
                mainViewModel.StatusReady();
            }
        }
        private bool ChangeTrayNumberCanExecute()
        {
            if (TrayOpened)
            {
                return false;
            }

            return true;
        }

        private async void AddStockEvent()
        {
            mainViewModel.StatusWait("Stock Confirming...");
            var trayId = SelectedTray.Id;
            //Logic to close tray
            NetworkApp.str = "-" + trayId.ToString();
            mainViewModel.TrayStatus = NetworkApp.rackside;
            //Once it closed
            await mainViewModel.ArticleService.AddArticleToTray(SelectedTray, SelectedArticle, ArticleQty);
            ChangeTrayNumberEvent(); //Refresh tray items
            mainViewModel.RequestViewModel.ChangeRequestOrderQty(SelectedArticle, ArticleQty);
            TrayOpened = false;
            ArticleQty = 0;
            AddStockCommand.RaiseCanExecuteChanged();
            mainViewModel.StatusReady();
        }


        private bool LoadingEventCanExecute()
        {
            var canExecute = true;

            if (mainViewModel.RequestViewModel.IsTrayOpended)
            {
                canExecute = false;
            }

            //if (mainViewModel.RequestViewModel.RequestOrderList != null)
            //{
            //    foreach (var item in mainViewModel.RequestViewModel.RequestOrderList)
            //    {
            //        if (item.ButtonText == "Close")
            //        {
            //            canExecute = false;
            //        }
            //    }
            //}

            return canExecute;
        }

        private void LoadingEvent()
        {
            ChangeTrayNumberEvent();
        }

        private bool AddStockEventCanExecute()
        {
            if (ArticleQty > 0 && TrayOpened && SelectedArticle != null)
            {
                return true;
            }

            return false;
        }

        private void CallTrayOpenEvent()
        {
            var trayId = SelectedTray.Id;
            //Logic to open the tray
            NetworkApp.str = trayId.ToString();
            mainViewModel.TrayStatus = NetworkApp.rackside;
            //Once it open
            TrayOpened = true;
            CallTrayOpenCommand.RaiseCanExecuteChanged();
            ChangeTrayNumberCommand.RaiseCanExecuteChanged();
            ArticleChangeCommand.RaiseCanExecuteChanged();
        }

        private bool CallTrayOpenEventCanExecute()
        {
            if (!TrayOpened && SelectedArticle != null)
            {
                return true;
            }

            return false;
        }

        private async void ArticleChangeEvent(Article article)
        {
            SelectedArticle = article;

            mainViewModel.StatusWait("Getting tray information on: " + SelectedArticle.Name);
            ArticleText = SelectedArticle.Name;
            var tray = await mainViewModel.ArticleService.GetTrayFromArticle(SelectedArticle);
            if (mainViewModel.Trays != null && tray != null)
            {
                SelectedTray = mainViewModel.Trays.Where(t => t.Id == tray.Id).FirstOrDefault();
            }
            ArticleChangeCommand.RaiseCanExecuteChanged();
            mainViewModel.StatusReady();
        }
        private bool ArticleChangeCanExecute(Article article)
        {
            if (TrayOpened)
            {
                return false;
            }

            return true;
        }


    }
}
