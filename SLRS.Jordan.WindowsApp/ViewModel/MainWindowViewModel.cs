﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using SLRS.Jordan.WindowsApp.Services;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace SLRS.Jordan.WindowsApp.ViewModel
{
    //TODO: divide viewmodel to separate classes
    class MainWindowViewModel : ViewModelBase
    {
        public ArticleService ArticleService { get; set; }
        public OrdersService OrdersService { get; set; }
        public TrayService TrayService { get; set; }

        protected string _status = "Ready";
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                RaisePropertyChanged(nameof(Status));
            }
        }

        protected Brush _statusColor = Brushes.CornflowerBlue;
        public Brush StatusColor
        {
            get
            {
                return _statusColor;
            }
            set
            {
                _statusColor = value;
                RaisePropertyChanged(nameof(StatusColor));
            }
        }

        protected string _statusIcon = "Images/ready.png";
        public string StatusIcon
        {
            get
            {
                return _statusIcon;
            }
            set
            {
                _statusIcon = value;
                RaisePropertyChanged(nameof(StatusIcon));
            }
        }

        public LoadingViewModel LoadingViewModel { get; set; }
        public RequestViewModel RequestViewModel { get; set; }

        private Brush _btnLoadingColor = Brushes.CornflowerBlue;
        public Brush BtnLoadingColor
        {
            get
            {
                return _btnLoadingColor;
            }
            set
            {
                _btnLoadingColor = value;
                RaisePropertyChanged(nameof(BtnLoadingColor));
            }
        }

        private string _trayStatus { get; set; }
        public string TrayStatus
        {
            get
            {
                return _trayStatus;
            }
            set
            {
                _trayStatus = value;
                RaisePropertyChanged(nameof(TrayStatus));
            }
        }

        private Brush _btnRequestColor = Brushes.LightGray;
        public Brush BtnRequestColor
        {
            get
            {
                return _btnRequestColor;
            }
            set
            {
                _btnRequestColor = value;
                RaisePropertyChanged(nameof(BtnRequestColor));
            }
        }

        private ObservableCollection<Tray> _tray = null;
        public ObservableCollection<Tray> Trays
        {
            set
            {
                _tray = value;
                RaisePropertyChanged(nameof(Trays));
            }
            get
            {
                return _tray;
            }
        }
     
        private ObservableCollection<Article> _articles;
        public ObservableCollection<Article> Articles
        {
            set
            {
                _articles = value;
                RaisePropertyChanged(nameof(Articles));
            }
            get
            {
                return _articles;
            }
        }

        private ObservableCollection<RequestOrderWithTotalQuantity> _loadedItems;
        public ObservableCollection<RequestOrderWithTotalQuantity> LoadedItems
        {
            get
            {
                return _loadedItems;
            }
            set
            {
                _loadedItems = value;
                RaisePropertyChanged(nameof(LoadedItems));
            }
        }

        public RelayCommand WindowLoadCommand { get; set; }

        public MainWindowViewModel()
        {
            ArticleService = new ArticleService();
            OrdersService = new OrdersService();
            TrayService = new TrayService();

            LoadingViewModel = new LoadingViewModel(this);
            RequestViewModel = new RequestViewModel(this);

            WindowLoadCommand = new RelayCommand(WindowLoad);
        }

        public async void WindowLoad()
        {
            StatusWait("Initializing");
            Articles = await ArticleService.GetArticles();
            Trays = await TrayService.GetTrays();
            LoadedItems = await TrayService.GetAllTrayItems();


            StatusReady();
        }


        public void StatusWait(string message = "Please wait")
        {
            StatusIcon = "Images/loading.png";
            StatusColor = Brushes.PaleVioletRed;
            Status = message;
        }

        public void StatusReady(string message = "Ready")
        {
            StatusIcon = "Images/ready.png";
            StatusColor = Brushes.CornflowerBlue;
            Status = message;
        }

    }
}
