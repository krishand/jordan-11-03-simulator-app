﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SLRS.Jordan.WindowsApp.Helpers
{
    class MyHttpClient<T>
    {
        HttpClient _httpClinet = new HttpClient();

        public async Task<T> GetAsync(string url)
        {
            string serverUrl = ConfigurationManager.AppSettings["ServerUrl"].ToString();

            HttpResponseMessage message = await _httpClinet.GetAsync(serverUrl + url);
            message.EnsureSuccessStatusCode();

            var resultString = message.Content.ReadAsStringAsync();
            var result = message.Content.ReadAsAsync<T>();

            return await result;
        }

        public async Task<T> PostAsync<TPost>(string url, TPost tpost)
        {
            string serverUrl = ConfigurationManager.AppSettings["ServerUrl"].ToString();

            HttpResponseMessage message = await _httpClinet.PostAsJsonAsync(serverUrl + url, tpost);
            message.EnsureSuccessStatusCode();

            var resultString = message.Content.ReadAsStringAsync();
            var result = message.Content.ReadAsAsync<T>();

            return await result;
        }

        public async Task<T> PutAsync<TPost>(string url, TPost tpost)
        {
            string serverUrl = ConfigurationManager.AppSettings["ServerUrl"].ToString();

            HttpResponseMessage message = await _httpClinet.PutAsJsonAsync(serverUrl + url, tpost);
            message.EnsureSuccessStatusCode();

            var resultString = message.Content.ReadAsStringAsync();
            var result = message.Content.ReadAsAsync<T>();

            return await result;
        }
    }
}
