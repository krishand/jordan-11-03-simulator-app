﻿using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLRS.Jordan.WindowsApp.Services
{
    class OrdersService
    {
        public async Task<ObservableCollection<RequestOrderWithTotalQuantity>> GetPendingOrders()
        {
            return await new MyHttpClient<ObservableCollection<RequestOrderWithTotalQuantity>>().GetAsync("/api/orders/pending");
        }

        public async Task<int> NewRequestOrder(Article requestArticle, int requestQty)
        {
            return await new MyHttpClient<int>().PostAsync<RequestOrder>("/api/orders/new", new RequestOrder()
            {
                Status = OrderStatus.Pending,
                ArticleId = requestArticle.Id,
                Quantity = requestQty,
            });
        }

    }
}
