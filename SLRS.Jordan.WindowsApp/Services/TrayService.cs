﻿using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLRS.Jordan.WindowsApp.Services
{
    class TrayService
    {
        public async Task<ObservableCollection<Tray>> GetTrays()
        {
            return await new MyHttpClient<ObservableCollection<Tray>>().GetAsync("/api/trays");
        }

        public async Task<ObservableCollection<TrayItem>> GetTrayItems(Tray tray)
        {
            return await new MyHttpClient<ObservableCollection<TrayItem>>()
                    .GetAsync(string.Format("/api/trays/{0}/items", tray.Id));
        }

        public async Task<AvailableArticleQuantity> TotalAvailableArticles(Article article)
        {
            return await new MyHttpClient<AvailableArticleQuantity>().GetAsync(string.Format("/api/articles/{0}/availableqty/", article.Id));
        }

        public async Task<int> ClosePendingTray(ClosingTrayItem closingTray)
        {
            //return await new MyHttpClient<int>().PutAsync<RequestOrder>("/api/orders/close/", requestOrderItem);
            return await new MyHttpClient<int>().PutAsync<ClosingTrayItem>("/api/orders/tray/close/", closingTray);
        }
    
        internal async Task<ObservableCollection<RequestOrderWithTotalQuantity>> GetAllTrayItems()
        {
            return await new MyHttpClient<ObservableCollection<RequestOrderWithTotalQuantity>>().GetAsync("/api/trays/alltrayitems");
        }


        internal async Task<int> CloseRequestOrder(RequestOrderWithTotalQuantity requestOrder)
        {
            return await new MyHttpClient<int>().PutAsync("/api/orders/close", requestOrder);
        }
    }
}
