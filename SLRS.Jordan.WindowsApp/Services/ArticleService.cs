﻿using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.WindowsApp.Helpers;
//using SLRS.Jordan.WindowsApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLRS.Jordan.WindowsApp.Services
{
    class ArticleService
    {
        public async Task<ObservableCollection<Article>> GetArticles()
        {
            return await new MyHttpClient<ObservableCollection<Article>>()
                .GetAsync("/api/articles/");

        }

        public async Task<Tray> GetTrayFromArticle(Article article)
        {
            return await new MyHttpClient<Tray>().GetAsync(string.Format("/api/articles/{0}/tray", article.Id));
        }

        public async Task<ObservableCollection<TrayItemsGroupByTray>> GetTrayListFromArticle(Article article)
        {
            return await new MyHttpClient<ObservableCollection<TrayItemsGroupByTray>>().GetAsync(string.Format("/api/articles/{0}/traylist", article.Id));
        }


        public async Task<int> AddArticleToTray(Tray tray, Article article, int Qty)
        {
            return await new MyHttpClient<int>().PostAsync<TrayItem>("/api/trays/create/trayitem/", new TrayItem()
            {
                ArticleId = article.Id,
                Quantity = Qty,
                TrayId = tray.Id
            });
        }
    }
}
