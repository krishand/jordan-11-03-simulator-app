﻿//using SLRS.Jordan.WindowsApp.Model;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Network;
using SLRS.Jordan.WindowsApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SLRS.Jordan.WindowsApp
{
    public partial class MainWindow
    {
        MainWindowViewModel _vm;
        public MainWindow()
        {
            InitializeComponent();
            _vm = (MainWindowViewModel)this.DataContext;
            //new NetworkApp(_vm);
        }

        private void SwitchToLoading(object sender, System.Windows.RoutedEventArgs e)
        {
            _vm.BtnLoadingColor = Brushes.CornflowerBlue;
            _vm.BtnRequestColor = Brushes.LightGray;
            tabPageLoading.IsSelected = true;
        }

        private void SwitchToRequest(object sender, System.Windows.RoutedEventArgs e)
        {
            _vm.BtnLoadingColor = Brushes.LightGray;
            _vm.BtnRequestColor = Brushes.CornflowerBlue;
            tabPageRequest.IsSelected = true;
        }

        private async void RequestOrderCommand(object sender, System.Windows.RoutedEventArgs e)
        {
            _vm.StatusWait();
            //disable all buttons
            var buttons = FindVisualChildren<Button>(RequestOrderList).ToList();
            foreach (var btn in buttons)
            {
                btn.IsEnabled = false;
            }
            var button = sender as Button;
            button.IsEnabled = true;
            var requestOrder = (RequestOrder)((ListBoxItem)RequestOrderList.ContainerFromElement(button)).Content;
            var article = _vm.Articles.Where(t => t.Id == requestOrder.ArticleId).First();
            var tray = await _vm.ArticleService.GetTrayFromArticle(article);
            //code for tray open
            NetworkApp.str = tray.Id.ToString();
            _vm.TrayStatus = NetworkApp.rackside;
            //once it open
            button.Content = "Close";
            button.Background = Brushes.MediumVioletRed;
            button.Click += RequestOrderCommand_Close;
            _vm.StatusReady();

        }

        private async void RequestOrderCommand_Close(object sender, RoutedEventArgs e)
        {
            //_vm.StatusWait();
            //var buttons = FindVisualChildren<Button>(RequestOrderList).ToList();
            //foreach (var btn in buttons)
            //{
            //    btn.IsEnabled = true;
            //}

            //var button = sender as Button;
            //var requestOrder = (RequestOrder)((ListBoxItem)RequestOrderList.ContainerFromElement(button)).Content;
            //var article = _vm.Articles.Where(t => t.Id == requestOrder.ArticleId).First();
            //var tray = await _vm.ArticleService.GetTrayFromArticle(article);
            ////code for tray close
            //NetworkApp.str = "-" + tray.Id.ToString();
            //_vm.TrayStatus = NetworkApp.rackside;
            ////once it closed
            //button.Content = "Open";
            //button.Background = Brushes.CornflowerBlue;
            //button.Click -= RequestOrderCommand_Close;

            ////await _vm.TrayService.CloseRequestOrder(requestOrder);
            //_vm.RequestViewModel.RequestOrderList = await _vm.OrdersService.GetPendingOrders();
            //_vm.StatusReady();
        }

        //should refactor
        private IEnumerable<T> FindVisualChildren<T>(DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is T)
                {
                    yield return (T)child;
                }
                else
                {
                    var childOfChild = FindVisualChildren<T>(child);
                    if (childOfChild != null)
                    {
                        foreach (var subchild in childOfChild)
                        {
                            yield return subchild;
                        }
                    }
                }
            }
        }

        private void btnCall_Click(object sender, RoutedEventArgs e)
        {
            //cmbItem.IsEnabled = false;
            cmbTray.IsEnabled = false;
            //txtQty.IsEnabled = false;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            //cmbItem.IsEnabled = true;
            cmbTray.IsEnabled = true;
            txtQty.IsEnabled = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }
    }
}
