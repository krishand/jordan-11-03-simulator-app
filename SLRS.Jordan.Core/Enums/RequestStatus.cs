﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.Enums
{
    public enum RequestStatus
    {
        Loaded = 1,
        Issued = 2,
        Returned = 3
    }
}
