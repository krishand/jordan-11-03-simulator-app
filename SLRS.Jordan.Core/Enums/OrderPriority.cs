﻿namespace SLRS.Jordan.Core.Enums
{
    public enum OrderPriority
    {
        High = 1,
        Low = 2
    }
}
