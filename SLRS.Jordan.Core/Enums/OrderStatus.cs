﻿namespace SLRS.Jordan.Core.Enums
{
    public enum OrderStatus: int
    {
        Pending = 1,
        Closed = 2,
        Cancelled = 3
    }
}
