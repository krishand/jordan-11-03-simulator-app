﻿using System;
namespace SLRS.Jordan.Core.DTO.Requests
{
    public class RequestedArticle
    {
        public int ArticleId { get; set; }
        public int NoOfArticlesNeeded { get; set; }
    }
}
