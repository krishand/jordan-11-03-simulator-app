﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.DTO.Requests
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        public int MachineNumber { get; set; }
        [Required]
        public string Priority { get; set; }
        [Required]
        public int ComponentId { get; set; }
        [Required]
        public string BatchNo { get; set; }

        public string Status { get; set; }

        public IEnumerable<RequestedArticle> Articles { get; set; }
    }
}
