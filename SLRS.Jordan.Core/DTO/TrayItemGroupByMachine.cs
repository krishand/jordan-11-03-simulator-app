﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayItemGroupByMachine
    {
        public List<TrayReferenceGroup> Machine1 { get; set; }
        public List<TrayReferenceGroup> Machine2 { get; set; }
    }

    public class TrayReferenceGroup
    {
        public int TrayId { get; set; }
        public List<TrayReference> TrayReference { get; set; }
    }
}
