﻿using SLRS.Jordan.Core.Models.Articles;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class ArticleWithTotal
    {
        public Article Article { get; set; }
        public double TotalWeight { get; set; }
        public int TotalQuantity { get; set; }
    }
}
