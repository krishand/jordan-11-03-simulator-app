﻿using SLRS.Jordan.Core.Models.LogiMat;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayItemWithTags
    {
        public TrayItem TrayItem { get; set; }
        public ArticleCount ArticleCount { get; set; }
    }
}
