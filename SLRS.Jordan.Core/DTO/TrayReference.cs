﻿using SLRS.Jordan.Core.Models.Articles;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayReference
    {
        public int ArticleId { get; set; }
        public Article Article { get; set; }
        public int RequestedQuantity { get; set; }
        public int GotQuantity { get; set; } //quantity that accually got for the article 
        public int TrayId { get; set; }
        public DateTime TrayItemDate { get; set; }
        public float GotWeight { get; set; }
    }
}
