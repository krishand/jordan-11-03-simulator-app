﻿using System;
namespace SLRS.Jordan.Core.DTO.Auth
{
    public class Role
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
