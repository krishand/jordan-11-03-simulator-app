﻿using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.DTO.Auth
{
    public class Login
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
