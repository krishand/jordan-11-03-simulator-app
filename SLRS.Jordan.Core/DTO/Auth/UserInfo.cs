﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.DTO.Auth
{
    public class UserInfo
    {
        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Role { get; set; }
    }
}
