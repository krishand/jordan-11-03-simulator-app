﻿using SLRS.Jordan.Core.Models.LogiMat;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayItemGroupByArticleWithTotalQuanity
    {
        public int ArticleId { get; set; }

        private int _totalQuantity;
        public int TotalQuantity
        {
            get
            {
                return _totalQuantity;
            }
            set
            {
                _totalQuantity = value;
                CanFulfillRequestedQuantity = (TotalQuantity > RequestedQuantity);
            }
        }

        public bool CanFulfillRequestedQuantity { get; set; }
        public List<TrayItem> TrayItemList { get; set; }
        public int RequestedQuantity { get; set; }

        public TrayItemGroupByArticleWithTotalQuanity(int requestedQuantity)
        {
            RequestedQuantity = requestedQuantity;
        }

    }
}
