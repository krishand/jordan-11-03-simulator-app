﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayItemGroupByTray
    {
        public int TrayId { get; set; }
        public int Priority { get; set; }
        public List<TrayItemGroupByArticleWithTotalQuanity> ArticleList { get; set; }
    }
}
