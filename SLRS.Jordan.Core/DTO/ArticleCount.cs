﻿using SLRS.Jordan.Core.Models.Articles;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class ArticleCount
    {
        public Article Article { get; set; }
        public List<string> Tag { get; set; }
        public int Count { get; set; }

        public ArticleCount()
        {
            Tag = new List<string>();
        }
    }
}
