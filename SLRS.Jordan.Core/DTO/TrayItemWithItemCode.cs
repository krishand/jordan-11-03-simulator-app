﻿using SLRS.Jordan.Core.Models.LogiMat;
using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayItemWithItemCode
    {
        public TrayItem TrayItem { get; set; }
        public List<ItemCode> ItemCodes { get; set; }
    }
}
