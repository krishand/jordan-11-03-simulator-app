﻿using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;

namespace SLRS.Jordan.Core.DTO
{
    public class TrayWithTotalArticleQuantity
    {
        public Article Article { get; set; }
        public Tray Tray { get; set; }

        public int TotalArticleQuantity { get; set; }
    }
}
