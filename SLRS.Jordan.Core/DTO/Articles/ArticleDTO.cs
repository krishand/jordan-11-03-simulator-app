﻿using System;
using System.Collections.Generic;

namespace SLRS.Jordan.Core.DTO.Articles
{
    public class ArticleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ArticleComponentDTO> Components { get; set; }
    }
}
