﻿using System;
namespace SLRS.Jordan.Core.DTO.Articles
{
    public class ArticleComponentDTO
    {
        public int ArticleId { get; set; }
        public int ComponentId { get; set; }
    }
}
