﻿using System;
namespace SLRS.Jordan.Core.Models.Articles
{
    public class ArticleComponent
    {
        public int ArticleId { get; set; }
        public Article Article { get; set; }

        public int ComponentId { get; set; }
        public Component Component { get; set; }
    }
}
