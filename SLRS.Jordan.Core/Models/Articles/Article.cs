﻿using SLRS.Jordan.Core.Models.LogiMat;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.Models.Articles
{
    public class Article
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ICollection<ArticleComponent> ArticleComponents { get; set; }

    }
}
