﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.Models.Articles
{
    public class Component
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ICollection<ArticleComponent> ArticleComponents { get; set; }
    }
}
