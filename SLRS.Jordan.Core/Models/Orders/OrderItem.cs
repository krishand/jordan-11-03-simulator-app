﻿using System.ComponentModel.DataAnnotations;
using SLRS.Jordan.Core.Models.Articles;

namespace SLRS.Jordan.Core.Models.Orders
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public virtual RequestOrder RequestOrder { get; set; }
        public int RequestOrderId { get; set; }

        [Required]
        public virtual Article Article { get; set; }
        public int ArticleId { get; set; }

    }
}
