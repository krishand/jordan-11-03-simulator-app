﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;

namespace SLRS.Jordan.Core.Models.Orders
{
    public class RequestOrder
    {
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string BatchNumber { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public OrderPriority OrderPriority { get; set; }

        [Required]
        public OrderStatus Status { get; set; }

        [Required]
        public DateTime RequestedTime { get; set; }

        [Required]
        public int DeviceId { get; set; }

        [Required]
        public int ComponentId { get; set; }
        public virtual Component Component { get; set; }

        [Required]
        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public RequestOrder()
        {
            OrderItems = new Collection<OrderItem>();
        }
    }
}
