﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class TrayItemItemCode
    {
        public int Id { get; set; }

        public TrayItem TrayItem { get; set; }
        public int TrayItemId { get; set; }

        public ItemCode ItemCode { get; set; }
        public int ItemCodeId { get; set; }

    }
}
