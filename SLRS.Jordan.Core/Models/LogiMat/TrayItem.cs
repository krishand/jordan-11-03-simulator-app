﻿using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class TrayItem
    {
        public int Id { get; set; }

        [Required]
        public int ArticleId { get; set; }
        public Article Article { get; set; }

        [Required]
        public int TrayId { get; set; }
        public Tray Tray { get; set; }

        [Required]
        public int Quantity { get; set; }

        public double Weight { get; set; }

        public RequestStatus Status { get; set; }

        public DateTime Date { get; set; }
    }
}
