﻿using SLRS.Jordan.Core.Models.Articles;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class ItemCode
    {
        public int Id { get; set; }

        public string RfidCode { get; set; }

        public Article Article { get; set; }
        public int ArticleId { get; set; }
    }
}
