﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class Machine
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public virtual ICollection<Tray> Trays { get; set; }

        
    }
}
