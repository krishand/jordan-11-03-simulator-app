﻿using SLRS.Jordan.Core.Models.Articles;
using System.ComponentModel.DataAnnotations;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class TrayMap
    {
        public int Id { get; set; }

        [Required]
        public int ArticleId { get; set; }

        [Required]
        public int TrayId { get; set; }

        //
        public Tray Tray { get; set; }
        public Article Article { get; set; }

    }
}
