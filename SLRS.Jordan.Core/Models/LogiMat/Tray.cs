﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SLRS.Jordan.Core.Models.LogiMat
{
    public class Tray
    {
        public int Id { get; set; }

        [Required]
        public int Number { get; set; }

        public Machine Machine { get; set; }
    }
}
