﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace JWA
{
    public partial class App : Application
    {

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Unexpected error occured");
            _logger.Log(new LogEventInfo()
            {
                Level = LogLevel.Error,
                Message = e.Exception.Message,
                Exception = e.Exception,
                LoggerName = "Unhandle Errors",
            });

            e.Handled = true;
        }
    }
}
