﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JWA.Helpers
{
    public class ListThings<T>
    {
        public List<T> RemoveDuplicateAdd(List<T> source, List<T> target)
        {
            var newItems = target.Where(t=>!source.Contains(t));
            
            foreach(var item in newItems)
            {
                source.Add(item);
            }

            return source;
        }
    }
}
