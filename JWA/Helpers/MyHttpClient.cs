﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using JWA.ViewModel;

namespace JWA.Helpers
{
    class MyHttpClient<T>
    {
        HttpClient _httpClient = new HttpClient();
        String _serverUrl;

        public MyHttpClient()
        {
            _serverUrl = ConfigurationManager.AppSettings["ServerUrl"].ToString();
        }

        public async Task<T> GetAsync(string url)
        {
            BaseViewModel.Current.ServiceStatus.StatusWorking();
            HttpResponseMessage message = await _httpClient.GetAsync(_serverUrl + url);
            return await Core(message);
        }

        public async Task<T> PostAsync<TPost>(string url, TPost tpost)
        {
            BaseViewModel.Current.ServiceStatus.StatusWorking();

            HttpResponseMessage message = await _httpClient.PostAsJsonAsync(_serverUrl + url, tpost);
            return await Core(message);
        }

        public async Task<T> PutAsync<TPost>(string url, TPost tpost)
        {
            BaseViewModel.Current.ServiceStatus.StatusWorking();

            HttpResponseMessage message = await _httpClient.PutAsJsonAsync(_serverUrl + url, tpost);
            return await Core(message);
        }


        private async Task<T> Core(HttpResponseMessage message)
        {
            message.EnsureSuccessStatusCode();

            var resultString = message.Content.ReadAsStringAsync();
            var result = message.Content.ReadAsAsync<T>();

            BaseViewModel.Current.ServiceStatus.StatusReady();

            return await result;
        }
    }
}