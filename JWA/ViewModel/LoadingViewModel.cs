﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using JWA.Helpers;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.WindowsApp.Network;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;

namespace JWA.ViewModel
{
    public class LoadingViewModel : ViewModelBase
    {
        private BaseViewModel _base;

        #region Properties
        private ObservableCollection<Article> _articleList;
        public ObservableCollection<Article> ArticleList
        {
            get
            {
                return _articleList;
            }
            set
            {
                _articleList = value; RaisePropertyChanged(nameof(ArticleList));
            }
        }

        private ObservableCollection<Article> _articleSearchResult;
        public ObservableCollection<Article> ArticleSearchResult
        {
            get
            {
                return _articleSearchResult;

            }
            set
            {
                _articleSearchResult = value;
                RaisePropertyChanged(nameof(ArticleSearchResult));
            }
        }

        private Article _selectedArticle;
        public Article SelectedArticle
        {
            get
            {
                return _selectedArticle;
            }
            set
            {
                _selectedArticle = value;
                RaisePropertyChanged(nameof(SelectArticleCommand));
            }
        }

        private string _searchString;
        public string SearchString
        {
            get
            {
                return _searchString;
            }
            set
            {
                _searchString = value;
                RaisePropertyChanged(nameof(SearchString));
            }
        }

        public int _articleCount;
        public int ArticleCount
        {
            get
            {
                return _articleCount;
            }
            set
            {
                _articleCount = value;
                RaisePropertyChanged(nameof(ArticleCount));
            }
        }

        public double _articleWeight;
        public double ArticleWeight
        {
            get
            {
                return _articleWeight;
            }
            set
            {
                _articleWeight = value;
                RaisePropertyChanged(nameof(ArticleWeight));
            }
        }

        private DateTime _inHouseDate = DateTime.Now;
        public DateTime InHouseDate
        {
            get
            {
                return _inHouseDate;
            }
            set
            {
                _inHouseDate = value;
                RaisePropertyChanged(nameof(InHouseDate));
            }
        }

        private List<int> _trayNumbers = new List<int>();
        public List<int> TrayNumbers
        {
            get
            {
                return _trayNumbers;
            }
            set
            {
                _trayNumbers = value;
                RaisePropertyChanged(nameof(TrayNumbers));
            }
        }

        private int _selectedTrayNumber;

        private List<TrayWithTotalArticleQuantity> _articleTrays;
        public List<TrayWithTotalArticleQuantity> ArticleTrays
        {
            get
            {
                return _articleTrays;
            }
            set
            {
                _articleTrays = value;
                RaisePropertyChanged(nameof(ArticleTrays));
            }
        }

        public int SelectedTrayNumber
        {
            get
            {
                return _selectedTrayNumber;
            }
            set
            {
                _selectedTrayNumber = value;
                RaisePropertyChanged(nameof(SelectedTrayNumber));
            }
        }

        public bool _isTrayOpened;
        public bool IsTrayOpened
        {
            get
            {
                return _isTrayOpened;
            }
            set
            {
                _isTrayOpened = value;
                RaisePropertyChanged(nameof(IsTrayOpened));
            }
        }

        private List<string> _articleRfidList = new List<string>();
        public List<string> ArticleRfidList
        {
            get
            {
                return _articleRfidList;
            }
            set
            {
                _articleRfidList = value;
                RaisePropertyChanged(nameof(ArticleRfidList));
            }
        }



        private bool _EditableVal;
        public bool EditableVal
        {
            get
            {
                return _EditableVal;
            }
            set
            {
                _EditableVal = value;
                RaisePropertyChanged(nameof(EditableVal));
            }
        }


        private string _callTrayButtonText;
        public string CallTrayButtonText
        {
            get
            {
                return _callTrayButtonText;
            }
            set
            {
                _callTrayButtonText = value;
                RaisePropertyChanged(nameof(CallTrayButtonText));
            }
        }

        private Brush _callTrayButtonColor = Brushes.DarkCyan;

        public Brush CallTrayButtonColor
        {
            get
            {
                return _callTrayButtonColor;
            }
            set
            {
                _callTrayButtonColor = value;
                RaisePropertyChanged(nameof(CallTrayButtonColor));
            }
        }

        public ObservableCollection<TrayItem> _addedTrayItems = new ObservableCollection<TrayItem>();
        public ObservableCollection<TrayItem> AddedTrayItems
        {
            get
            {
                return _addedTrayItems;
            }
            set
            {
                _addedTrayItems = value;
                RaisePropertyChanged(nameof(AddedTrayItems));
            }
        }
        #endregion

        #region Events
        public RelayCommand ControlLoadedCommand { get; set; }
        public RelayCommand<Article> SelectArticleCommand { get; set; }
        public RelayCommand ScanWeightRfidCommand { get; set; }
        public RelayCommand SearchArticleCommand { get; set; }
        public RelayCommand CallReturnTrayCommand { get; set; }
        public RelayCommand ClearUiCommand { get; set; }
        public RelayCommand AddItemToTrayCommand { get; set; }
        public RelayCommand<TrayWithTotalArticleQuantity> SelectTrayNumber { get; set; }
        #endregion

        public LoadingViewModel(BaseViewModel baseVm)
        {
            _base = baseVm;

            ControlLoadedCommand = new RelayCommand(ControlLoadedEvent);
            SelectArticleCommand = new RelayCommand<Article>(SelectArticleEvent);
            ScanWeightRfidCommand = new RelayCommand(ScanWeightRfidEvent, ScanWeightRfidCanExecute);
            SearchArticleCommand = new RelayCommand(ArticleSearchEvent, ArticleSearchCanExecute);
            CallReturnTrayCommand = new RelayCommand(CallReturnEvent, CallReturnCanExecute);
            ClearUiCommand = new RelayCommand(ClearUiEvent, ClearUiCanExecute);
            AddItemToTrayCommand = new RelayCommand(AddItemToTrayEvent, AddItemToTrayCanExecute);
            SelectTrayNumber = new RelayCommand<TrayWithTotalArticleQuantity>(SelectTrayNumberEvent, SelectTrayNumberCanExecute);

            ResetUI();
        }

        public void Timer_Tick(object sender, EventArgs e)
        {
            InHouseDate = DateTime.Now;
            ScanWeightRfidEvent();
        }

        private void SelectTrayNumberEvent(TrayWithTotalArticleQuantity trayWithQuanity)
        {

            SelectedTrayNumber = trayWithQuanity.Tray.Number;
            
        }

        private bool SelectTrayNumberCanExecute(TrayWithTotalArticleQuantity trayWithQuanity)
        {
            if (IsTrayOpened)
            {
                EditableVal = false;
            }
            else
            {
                EditableVal = true;
            }

            return true;
        }

        private bool AddItemToTrayCanExecute()
        {
            return IsTrayOpened;
        }

        private async void AddItemToTrayEvent()
        {
            if(ArticleWeight == 0)
            {
                return;
            }
            var trayItem = new TrayItem()
            {
                ArticleId = SelectedArticle.Id,
                Date = InHouseDate,
                Quantity = ArticleCount,
                Weight = ArticleWeight,
                Status = RequestStatus.Loaded,
                TrayId = SelectedTrayNumber,
                Article = SelectedArticle,
            };

            List<ItemCode> itemCodes = new List<ItemCode>();
            foreach (var rfid in ArticleRfidList)
            {
                itemCodes.Add(new ItemCode()
                {
                    RfidCode = rfid,
                    ArticleId = SelectedArticle.Id,
                });
            }

            AddedTrayItems.Add(new TrayItem()
            {
                Article = SelectedArticle,
                Quantity = ArticleCount,
            });
            CollectionViewSource.GetDefaultView(AddedTrayItems).Refresh();

            trayItem.Article = null;
            var trayItemWithItemCode = new TrayItemWithItemCode()
            {
                ItemCodes = itemCodes,
                TrayItem = trayItem,
            };

            await _base.databaseService.PostNewArticles(trayItemWithItemCode);

            GetArticleList();
        }

        private bool ClearUiCanExecute()
        {
            return true;
        }

        private void ClearUiEvent()
        {
            ResetUI();
        }

        private bool CallReturnCanExecute()
        {

                return (SelectedArticle != null && ArticleWeight != 0 && ArticleCount != 0 && SelectedTrayNumber != 0);
        }

        private void CallReturnEvent()
        {
            bool val = false;
            if (IsTrayOpened)
            {
                val = _base.TrayService.CloseTray(SelectedTrayNumber);
                if (val)
                {
                    IsTrayOpened = false;
                    ResetUI();
                    AddedTrayItems = new ObservableCollection<TrayItem>();
                }
                else
                {
                    System.Windows.MessageBox.Show("Try Again");
                }



                //tray close command
                //IsTrayOpened = false;
                //_base.TrayService.CloseTray(SelectedTrayNumber);
                //ResetUI();
                //AddedTrayItems = new ObservableCollection<TrayItem>(); ///finally added item/////2018-09-20
            }
            else
            {
                val = _base.TrayService.OpenTray(SelectedTrayNumber);

                if (val)
                {
                    CallTrayButtonColor = Brushes.MediumVioletRed;
                    CallTrayButtonText = "Return Tray";
                    //tray open command
                    IsTrayOpened = true;
                }
                else
                {
                    IsTrayOpened = false;
                    System.Windows.MessageBox.Show("Try Again");
                }

                
            }
        }

        private bool ArticleSearchCanExecute()
        {
            return (ArticleList != null);
        }

        private void ArticleSearchEvent()
        {
            if (SearchString != null)
            {
                var x = new ObservableCollection<Article>(ArticleList.Where(t => t.Name.ToLower().Contains(SearchString.ToLower())).Select(t => t));

                ArticleSearchResult = (x.Count != ArticleList.Count) ? x : null;
            }

        }

        private bool ScanWeightRfidCanExecute()
        {
            return (SelectedArticle != null);
        }

        bool weightReading = false;
        private async void ScanWeightRfidEvent()
        {
            if (weightReading)
            {
                return;
            }
            weightReading = true;
            var weight = _base.WeightScaler.Read(ArticleWeight);
            weightReading = false;

            if(weight != ArticleWeight)
            {
                ArticleWeight = weight;

                var newRfid = _base.RfidScanner.Read();
                //ArticleRfidList = new ListThings<string>().RemoveDuplicateAdd(ArticleRfidList, newRfid);
                ArticleRfidList = newRfid;

                //ArticleCount = ArticleRfidList.Count();
            }

        }

        private async void SelectArticleEvent(Article article)
        {
            SelectedArticle = article;
            SearchString = SelectedArticle.Name;

            var tray = await _base.databaseService.GetTrayFromArticle(article.Id);
            ArticleTrays = await _base.databaseService.GetAllTraysFromArticle(article.Id);
            if(tray != null)
            {
                SelectedTrayNumber = tray.Number;
            }


        }

        private void ControlLoadedEvent()
        {
            GetArticleList();
        }

        public async void GetArticleList()
        {
            ArticleList = await _base.databaseService.GetAllArticles();
        }

        public void ResetUI()
        {
            CallTrayButtonColor = Brushes.DarkCyan;
            CallTrayButtonText = "Call Tray";

            if (TrayNumbers.Count == 0)
            {
                TrayNumbers = new List<int>();
                for (var x = 0; x <= 120; x++)
                {
                    TrayNumbers.Add(x);
                }
            }

            SelectedTrayNumber = 0;

            ArticleCount = 0;
            ArticleRfidList = new List<string>();
            ArticleWeight = 0;
            //SearchString = "";
        }
    }
}
