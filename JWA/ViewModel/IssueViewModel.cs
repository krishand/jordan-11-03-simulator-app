﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using JWA.Helpers;
using JWA.Model;
using JWA.Services;
using JWA.View;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Network;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace JWA.ViewModel
{
    public class IssueViewModel : ViewModelBase
    {
        BaseViewModel _base;

        #region Properties
        List<ArticleCount> ArticleCountList = new List<ArticleCount>();

        public bool IsViewLoaded = false;

        bool _isTray1Opened = false;
        public bool IsTray1Opened
        {
            get
            {
                return _isTray1Opened;
            }
            set
            {
                _isTray1Opened = value;
                RaisePropertyChanged(nameof(IsTray1Opened));
            }
        }

        bool _isTray2Opened = false;
        public bool IsTray2Opened
        {
            get
            {
                return _isTray2Opened;
            }
            set
            {
                _isTray2Opened = value;
                RaisePropertyChanged(nameof(IsTray2Opened));
            }
        }

        bool _isScanning = false;
        public bool IsScanning
        {
            get
            {
                return _isScanning;
            }
            set
            {
                _isScanning = value;
                RaisePropertyChanged(nameof(IsScanning));
            }
        }
        
        //public double _articleWeight;
        //public double ArticleWeight
        //{
        //    get
        //    {
        //        return _articleWeight;
        //    }
        //    set
        //    {
        //        _articleWeight = value;
        //        RaisePropertyChanged(nameof(ArticleWeight));
        //    }
        //}
     
        private ObservableCollection<RequestOrderGrid> _pendingRequestOrders;
        public ObservableCollection<RequestOrderGrid> PendingRequestOrders
        {
            get
            {
                return _pendingRequestOrders;
            }
            set
            {
                _pendingRequestOrders = value;
                RaisePropertyChanged(nameof(PendingRequestOrders));
            }
        }

        private RequestOrderGrid _selectedRequestOrder;
        public RequestOrderGrid SelectedRequestOrder
        {
            get
            {
                return _selectedRequestOrder;
            }
            set
            {
                _selectedRequestOrder = value;
                RaisePropertyChanged(nameof(SelectedRequestOrder));
            }
        }

        private TrayItemGroupByMachine _selectedRequestGroupByMachine;
        public TrayItemGroupByMachine SelectedRequestGroupByMachine
        {
            get
            {
                return _selectedRequestGroupByMachine;
            }
            set
            {
                _selectedRequestGroupByMachine = value;
                RaisePropertyChanged(nameof(SelectedRequestGroupByMachine));
            }
        }

        private Visibility _machine1Visibility = Visibility.Hidden;
        public Visibility Machine1Visibility
        {
            get
            {
                return _machine1Visibility;
            }
            set
            {
                _machine1Visibility = value;
                RaisePropertyChanged(nameof(Machine1Visibility));
            }
        }

        private Visibility _machine2Visibility = Visibility.Hidden;
        public Visibility Machine2Visibility
        {
            get
            {
                return _machine2Visibility;
            }
            set
            {
                _machine2Visibility = value;
                RaisePropertyChanged(nameof(Machine2Visibility));
            }
        }

        private int _machine1TrayId;
        public int Machine1TrayId
        {
            get
            {
                return _machine1TrayId;
            }
            set
            {
                _machine1TrayId = value;
                RaisePropertyChanged(nameof(Machine1TrayId));
            }
        }

        private int _machine2TrayId;
        public int Machine2TrayId
        {
            get
            {
                return _machine2TrayId;
            }
            set
            {
                _machine2TrayId = value;
                RaisePropertyChanged(nameof(Machine2TrayId));
            }
        }

        private ObservableCollection<TrayReference> _requestOrderForMachine1;
        public ObservableCollection<TrayReference> RequestOrderForMachine1
        {
            get
            {
                if (_requestOrderForMachine1 == null)
                {
                    _requestOrderForMachine1 = new ObservableCollection<TrayReference>();
                }
                return _requestOrderForMachine1;
            }
            set
            {
                _requestOrderForMachine1 = value;
                RaisePropertyChanged(nameof(RequestOrderForMachine1));
            }
        }

        private ObservableCollection<TrayReference> _requestOrderForMachine2;
        public ObservableCollection<TrayReference> RequestOrderForMachine2
        {
            get
            {
                if (_requestOrderForMachine2 == null)
                {
                    _requestOrderForMachine2 = new ObservableCollection<TrayReference>();
                }
                return _requestOrderForMachine2;
            }
            set
            {
                _requestOrderForMachine2 = value;
                RaisePropertyChanged(nameof(RequestOrderForMachine2));
            }
        }

        private int _currentOrderItemNumber;
        public int CurrentOrderItemNumber
        {
            get
            {
                return _currentOrderItemNumber;
            }
            set
            {
                _currentOrderItemNumber = value;
                RaisePropertyChanged(nameof(CurrentOrderItemNumber));
            }
        }

        //private Brush _trayBackground;
        //public Brush TrayBackground
        //{
        //    get
        //    {
        //        return _trayBackground;
        //    }
        //    set
        //    {
        //        _trayBackground = value;
        //        RaisePropertyChanged(nameof(TrayBackground));
        //    }
        //}

        //private string _trayButtonText;
        //public string TrayButtonText
        //{
        //    get
        //    {
        //        return _trayButtonText;
        //    }
        //    set
        //    {
        //        _trayButtonText = value;
        //        RaisePropertyChanged(nameof(TrayButtonText));
        //    }
        //}

        public List<string> RfidTags = new List<string>();
        #endregion

        #region Events
        public RelayCommand ControlLoadedCommand { get; set; }
        public RelayCommand<RequestOrderGrid> RequestOpenCommand { get; set; }
        public RelayCommand NextOrderItemCommand { get; set; }
        public RelayCommand PrevOrderItemCommand { get; set; }
        public RelayCommand<RequestOrderGrid> ShowOrderItemsCommand { get; set; }
        public RelayCommand TrayOpenCommand { get; set; }
        public RelayCommand ReadRfidCommand { get; set; }
        public RelayCommand CloseRequestCommand { get; set; }
        public RelayCommand ResetUiCommand { get; set; }
        public RelayCommand<TrayType> Tray1CloseCommand { get; set; }
        public RelayCommand<TrayType> Tray2CloseCommand { get; set; }
        public RelayCommand ScanWeightRfidCommand { get; set; }
        public RelayCommand<Article> SelectArticleCommand { get; set; }
        public RelayCommand<TrayReference> GetArticleWeightCommand { get; set; }

        #endregion

        public IssueViewModel(BaseViewModel baseVm)
        {
            _base = baseVm;
            RequestOpenCommand = new RelayCommand<RequestOrderGrid>(RequestOpenEvent, RequestOpenCommandCanExecute);
            PrevOrderItemCommand = new RelayCommand(PrevOrderItemEvent, PrevOrderItemCanExecute);
            NextOrderItemCommand = new RelayCommand(NextOrderItemEvent, NextOrderItemCanExecute);
            ControlLoadedCommand = new RelayCommand(ControlLoadedEvent);
            ShowOrderItemsCommand = new RelayCommand<RequestOrderGrid>(ShowOrderItemEvent);
            TrayOpenCommand = new RelayCommand(TrayOpenEvent, TrayOpenCanExecute);
            Tray1CloseCommand = new RelayCommand<TrayType>(TrayCloseEvent, Tray1CloseCanExecute);
            Tray2CloseCommand = new RelayCommand<TrayType>(TrayCloseEvent, Tray2CloseCanExecute);
            ReadRfidCommand = new RelayCommand(ReadRfidEvent, ReadRfidCanExecute);
            CloseRequestCommand = new RelayCommand(CloseRequestEvent, CloseRequestCanExecute);
            ResetUiCommand = new RelayCommand(ResetScanned, ResetUiCanExecute);
            ScanWeightRfidCommand = new RelayCommand(ScanWeightRfidEvent, ScanWeightRfidCanExecute);
            GetArticleWeightCommand = new RelayCommand<TrayReference>(GetArticleWeightEvent, GetArticleWeightCanExecute);
            Reset();
        }

        private bool GetArticleWeightCanExecute(TrayReference trayReference)
        {
            return true;
        }

        private async void GetArticleWeightEvent(TrayReference trayReference)
        {
            trayReference.GotWeight = (float)  _base.WeightScaler.Read(trayReference.GotWeight);

            CollectionViewSource.GetDefaultView(RequestOrderForMachine1).Refresh();
            CollectionViewSource.GetDefaultView(RequestOrderForMachine2).Refresh();
        }

        private bool RequestOpenCommandCanExecute(RequestOrderGrid arg)
        {
            return !IsTray1Opened && !IsTray2Opened;
        }

        private bool Tray2CloseCanExecute(TrayType number)
        {
            return IsTray2Opened && Machine2Visibility == Visibility.Visible;
        }

        private bool Tray1CloseCanExecute(TrayType number)
        {
            return IsTray1Opened && Machine1Visibility == Visibility.Visible;
        }

        private List<string> _articleRfidList = new List<string>();
        public List<string> ArticleRfidList
        {
            get
            {
                return _articleRfidList;
            }
            set
            {
                _articleRfidList = value;
                RaisePropertyChanged(nameof(ArticleRfidList));
            }
        }

        private async void TrayCloseEvent(TrayType number)
        {
            var trayItemWithTagsList = new List<TrayItemWithTags>();

            if (number == TrayType.Tray1) //tray 1
            {
                foreach (var x in RequestOrderForMachine1)
                {
                    var trayItem = new TrayItemWithTags()
                    {
                        TrayItem = new TrayItem()
                        {
                            ArticleId = x.ArticleId,
                            Date = DateTime.Now,
                            Quantity = x.GotQuantity,
                            //Weight = Weight,
                            Weight = x.GotWeight,
                            Status = RequestStatus.Issued,
                            TrayId = Machine1TrayId
                        },

                        ArticleCount = ArticleCountList.Where(t => t.Article.Id == x.ArticleId).FirstOrDefault(),
                    };

                    trayItemWithTagsList.Add(trayItem);
                }
            }
            else if (number == TrayType.Tray2) //tray 2 
            {
                foreach (var x in RequestOrderForMachine2)
                {
                    var trayItem = new TrayItemWithTags()
                    {
                        TrayItem = new TrayItem()
                        {
                            ArticleId = x.ArticleId,
                            Date = DateTime.Now,
                            Quantity = x.GotQuantity,
                            Weight = x.GotWeight,
                            Status = RequestStatus.Issued,
                            TrayId = Machine2TrayId
                        },

                        ArticleCount = ArticleCountList.Where(t => t.Article.Id == x.ArticleId).FirstOrDefault(),
                    };

                    trayItemWithTagsList.Add(trayItem);
                }
            }

            await _base.databaseService.UpdateIssue(trayItemWithTagsList);

            if (number == TrayType.Tray1)
            {
                _base.TrayService.CloseTray(Machine1TrayId);
                IsTray1Opened = false;
            }
            else if (number == TrayType.Tray2)
            {
                _base.TrayService.CloseTray(Machine2TrayId);
                IsTray2Opened = false;
            }

            if (!IsTray1Opened && !IsTray2Opened)
            {
                ResetScanned();
            }

            CommandManager.InvalidateRequerySuggested();
        }

        private async void ControlLoadedEvent()
        {
            PendingRequestOrders = await _base.databaseService.GetPendingRequestOrdersAsync();
        }

        public void Timer_Tick(object sender, EventArgs e)
        {
            if (IsViewLoaded)
            {
                //var newWeight = _base.WeightScaler.Read();
                //if (newWeight != Weight)
                //{
                //    Weight = newWeight;
                //    ReadRfidEvent();
                //}
            }

        }

        private bool ResetUiCanExecute()
        {
            return true;
        }

        private void ResetScanned()
        {
            RfidTags = new List<string>();
            foreach (var item in RequestOrderForMachine1)
            {
                item.GotQuantity = 0;
            }
            foreach (var item in RequestOrderForMachine2)
            {
                item.GotQuantity = 0;
            }

            CollectionViewSource.GetDefaultView(RequestOrderForMachine1).Refresh();
            CollectionViewSource.GetDefaultView(RequestOrderForMachine2).Refresh();

        }

        private bool CloseRequestCanExecute()
        {
            return SelectedRequestOrder != null;
        }

        private async void CloseRequestEvent()
        {
            //TODO: close tray in database

            PendingRequestOrders.Remove(SelectedRequestOrder);
            PendingRequestOrders = PendingRequestOrders;

            var result = await _base.databaseService.CloseRequest(SelectedRequestOrder);
            if (result)
            {
                Reset();
            }
        }

        private List<TrayWithTotalArticleQuantity> _articleTrays;
        public List<TrayWithTotalArticleQuantity> ArticleTrays
        {
            get
            {
                return _articleTrays;
            }
            set
            {
                _articleTrays = value;
                RaisePropertyChanged(nameof(ArticleTrays));
            }
        }

        private Article _selectedArticle;
        public Article SelectedArticle
        {
            get
            {
                return _selectedArticle;
            }
            set
            {
                _selectedArticle = value;
                RaisePropertyChanged(nameof(SelectArticleCommand));
            }
        }

        private string _searchString;
        public string SearchString
        {
            get
            {
                return _searchString;
            }
            set
            {
                _searchString = value;
                RaisePropertyChanged(nameof(SearchString));
            }
        }

        private async void SelectArticleEvent(Article article)
        {
            SelectedArticle = article;
            SearchString = SelectedArticle.Name;

            var tray = await _base.databaseService.GetTrayFromArticle(article.Id);
            ArticleTrays = await _base.databaseService.GetAllTraysFromArticle(article.Id);
            if (tray != null)
            {
                SelectedTrayNumber = tray.Number;
            }


        }

        private int _selectedTrayNumber;

       
        public int SelectedTrayNumber
        {
            get
            {
                return _selectedTrayNumber;
            }
            set
            {
                _selectedTrayNumber = value;
                RaisePropertyChanged(nameof(SelectedTrayNumber));
            }
        }

        private async void ReadRfidEvent()
        {
            if (SelectedRequestGroupByMachine == null)
                return;

            IsScanning = true;

            var result = _base.RfidScanner.Read();

            var isValide = await _base.databaseService.ValidateSameArticles(result);
            if (isValide)
            {
                RfidTags = new ListThings<string>().RemoveDuplicateAdd(RfidTags, result);
                ArticleCountList = await _base.databaseService.GetArticleWithTags(RfidTags);

                foreach (var item in ArticleCountList)
                {
                    var machine1Article = SelectedRequestGroupByMachine.Machine1.Where(t => t.TrayReference.Where(t1 => t1.ArticleId == item.Article.Id).Any()).FirstOrDefault();
                    var machine2Article = SelectedRequestGroupByMachine.Machine2.Where(t => t.TrayReference.Where(t1 => t1.ArticleId == item.Article.Id).Any()).FirstOrDefault();

                    int machine1TotalArticleCount = 0;
                    int machine2TotalArticleCount = 0;

                    if (machine1Article != null)
                    {
                        machine1TotalArticleCount = machine1Article.TrayReference.Select(t => t.ArticleId).Count();
                        machine1Article.TrayReference.Where(t => t.ArticleId == item.Article.Id).First().GotQuantity = item.Count;
                    }

                    if (machine2Article != null)
                    {
                        machine2TotalArticleCount = machine2Article.TrayReference.Select(t => t.ArticleId).Count();
                        machine2Article.TrayReference.Where(t => t.ArticleId == item.Article.Id).First().GotQuantity = item.Count;
                    }

                    var totalScannedArticles = ArticleCountList.Select(t => t.Article).Count();

                    if (totalScannedArticles > machine1TotalArticleCount + machine2TotalArticleCount)
                    {
                        _base.ServiceStatus.StatusWarning("There are articles scanned that are not in the request, Please verify again");
                    }
                }

                SelectedRequestGroupByMachine = SelectedRequestGroupByMachine;
                DevideRequestOrderByMachine();

                IsScanning = false;

                CommandManager.InvalidateRequerySuggested();
            }
        }


        private bool ReadRfidCanExecute()
        {
            if (!IsTray1Opened && !IsTray2Opened)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void TrayOpenEvent()
        {

            if(Machine1Visibility == Visibility.Visible)
            {
                IsTray1Opened = true;
                _base.TrayService.OpenTray(Machine1TrayId);

            }
            if(Machine2Visibility == Visibility.Visible)
            {
                IsTray2Opened = true;
                _base.TrayService.OpenTray(Machine2TrayId);
            }


            //if (!IsTray1Opened && !IsTray2Opened)
            //{

            //    //TrayBackground = Brushes.CornflowerBlue;
            //    //TrayButtonText = "Open Tray";
            //}
            //else
            //{
            //    //TrayButtonText = "Close Tray";
            //    //TrayBackground = Brushes.MediumVioletRed;
            //    IsTray1Opened = false;
            //    IsTray2Opened = false;

            //    _base.TrayService.OpenTray(Machine1TrayId);
            //    _base.TrayService.OpenTray(Machine2TrayId);

            //}

            CommandManager.InvalidateRequerySuggested();

        }


        private bool TrayOpenCanExecute()
        {
            return SelectedRequestOrder != null && SelectedRequestGroupByMachine != null && !IsTray1Opened && !IsTray2Opened;
        }

        private void ShowOrderItemEvent(RequestOrderGrid requestOrder)
        {
            requestOrder.ArticleListGridStatus = (requestOrder.ArticleListGridStatus == Visibility.Collapsed) ? Visibility.Visible : Visibility.Collapsed;

            CollectionViewSource.GetDefaultView(PendingRequestOrders).Refresh();
        }

        private bool PrevOrderItemCanExecute()
        {
            return (SelectedRequestOrder != null && CurrentOrderItemNumber > 0 && SelectedRequestGroupByMachine != null && !IsTray1Opened && !IsTray2Opened);
        }

        private void PrevOrderItemEvent()
        {
            if (CurrentOrderItemNumber != 0)
            {
                CurrentOrderItemNumber--;
            }

            SelectCurrentRequestGroup();
        }

        private bool NextOrderItemCanExecute()
        {
            return (SelectedRequestOrder != null && CurrentOrderItemNumber < (MaxOrderCount() - 1) && SelectedRequestGroupByMachine != null && !IsTray1Opened && !IsTray2Opened);
        }

        private void NextOrderItemEvent()
        {
            if (CurrentOrderItemNumber < SelectedRequestOrder.OrderItems.Count - 1)
            {
                CurrentOrderItemNumber++;
            }
            SelectCurrentRequestGroup();
        }

        public int _articleCount;
        public int ArticleCount
        {
            get
            {
                return _articleCount;
            }
            set
            {
                _articleCount = value;
                RaisePropertyChanged(nameof(ArticleCount));
            }
        }
        private bool ScanWeightRfidCanExecute()
        {
            return (SelectedArticle != null);
        }

        private void ScanWeightRfidEvent()
        {
            //var weight = _base.WeightScaler.Read();

            //if (weight != ArticleWeight)
            //{
            //    ArticleWeight = weight;

            //    var newRfid = _base.RfidScanner.Read();
            //    //ArticleRfidList = new ListThings<string>().RemoveDuplicateAdd(ArticleRfidList, newRfid);
            //    ArticleRfidList = newRfid;

            //    ArticleCount = ArticleRfidList.Count();
            //}

        }

        private async void RequestOpenEvent(RequestOrderGrid requestOrder)
        {
            SelectedRequestOrder = requestOrder;
            SelectedRequestGroupByMachine = await _base.databaseService.GetRequestOrderGroupByMachine(requestOrder.Id);

            Machine1Visibility = (SelectedRequestGroupByMachine.Machine1.Count > 0) ? Visibility.Visible : Visibility.Hidden;
            Machine2Visibility = (SelectedRequestGroupByMachine.Machine2.Count > 0) ? Visibility.Visible : Visibility.Hidden;

            DevideRequestOrderByMachine();

            // Refresh all button's 'can execute'
            CommandManager.InvalidateRequerySuggested();
        }

        public void DevideRequestOrderByMachine()
        {
            if (SelectedRequestGroupByMachine != null)
            {
                var machine1Count = SelectedRequestGroupByMachine.Machine1.Count;
                RequestOrderForMachine1 = (machine1Count > 0) ? new ObservableCollection<TrayReference>(SelectedRequestGroupByMachine.Machine1[0].TrayReference) : null;
                Machine1TrayId = (machine1Count > 0 && CurrentOrderItemNumber > machine1Count) ? SelectedRequestGroupByMachine.Machine1[CurrentOrderItemNumber].TrayId : 0;

                var machine2Count = SelectedRequestGroupByMachine.Machine2.Count;
                RequestOrderForMachine2 = (machine2Count > 0) ? new ObservableCollection<TrayReference>(SelectedRequestGroupByMachine.Machine2[0].TrayReference) : null;
                Machine2TrayId = (machine2Count > 0 && CurrentOrderItemNumber > machine2Count) ? SelectedRequestGroupByMachine.Machine2[CurrentOrderItemNumber].TrayId : 0;

                SelectCurrentRequestGroup();
            }

        }

        public int MaxOrderCount()
        {
            var maxOrderCount = 0;

            if (SelectedRequestGroupByMachine != null)
            {
                if (SelectedRequestGroupByMachine.Machine1.Count > SelectedRequestGroupByMachine.Machine2.Count)
                {
                    maxOrderCount = SelectedRequestGroupByMachine.Machine1.Count;
                }
                else
                {
                    maxOrderCount = SelectedRequestGroupByMachine.Machine2.Count;
                }
            }

            return maxOrderCount;
        }

        public void Reset()
        {
            SelectedRequestOrder = null;
            CurrentOrderItemNumber = 0;
            Machine1Visibility = Visibility.Collapsed;
            Machine2Visibility = Visibility.Collapsed;
            SelectedRequestGroupByMachine = null;
            RequestOrderForMachine1 = new ObservableCollection<TrayReference>();
            RequestOrderForMachine2 = new ObservableCollection<TrayReference>();

            //TrayBackground = Brushes.CornflowerBlue;
            //TrayButtonText = "Open Tray";
        }

        public void SelectCurrentRequestGroup()
        {
            //for machine 1
            if (SelectedRequestGroupByMachine.Machine1.Count > CurrentOrderItemNumber)
            {
                RequestOrderForMachine1 = new ObservableCollection<TrayReference>(SelectedRequestGroupByMachine.Machine1[CurrentOrderItemNumber].TrayReference);
                Machine1TrayId = SelectedRequestGroupByMachine.Machine1[CurrentOrderItemNumber].TrayId;
                Machine1Visibility = Visibility.Visible;
            }
            else
            {
                Machine1Visibility = Visibility.Collapsed;
            }

            //for machine 2
            if (SelectedRequestGroupByMachine.Machine2.Count > CurrentOrderItemNumber)
            {
                RequestOrderForMachine2 = new ObservableCollection<TrayReference>(SelectedRequestGroupByMachine.Machine2[CurrentOrderItemNumber].TrayReference);
                Machine2TrayId = SelectedRequestGroupByMachine.Machine2[CurrentOrderItemNumber].TrayId;

                Machine2Visibility = Visibility.Visible;
            }
            else
            {
                Machine2Visibility = Visibility.Collapsed;
            }
        }
    }
}
