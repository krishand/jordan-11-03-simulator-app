﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using JWA.Services;
using JWA.View;
using SLRS.Jordan.Core;
using SLRS.Jordan.Core.Models.Orders;
using SLRS.Jordan.WindowsApp.Network;

namespace JWA.ViewModel
{

    public class BaseViewModel : ViewModelBase
    {

        #region Properties
        public string TrayStatus { get; set; }

        public string ActiveView { get; set; }

        public static BaseViewModel Current;
        public RfidScanner RfidScanner;
        public WeightScaler WeightScaler;

        // Services
        public DatabaseService databaseService = new DatabaseService();

        // View models
        public IssueViewModel IssueViewModel { get; set; }
        public LoadingViewModel LoadingViewModel { get; set; }
        public ItemReturnViewModel ItemReturnViewModel { get; set; }
        public UILoaderViewModel UILoaderViewModel { get; set; }

        private UserControl _loadingView;
        public UserControl LoadingView
        {
            get
            {
                return _loadingView;
            }
            set
            {
                _loadingView = value;
                RaisePropertyChanged(nameof(LoadingView));
            }
        }

        public ServiceStatus _serviceStatus;
        public ServiceStatus ServiceStatus
        {
            get
            {
                return _serviceStatus;
            }
            set
            {
                _serviceStatus = value;
                RaisePropertyChanged(nameof(ServiceStatus));
            }
        }
        public TrayService TrayService { get; private set; }
        #endregion

        #region Events
        public RelayCommand<Button> OrderLoadingCommand { get; set; }
        public RelayCommand WindowLoadedCommand { get; set; }
        public RelayCommand<string> OpenReturnWindow { get; set; }
        #endregion


        public BaseViewModel()
        {
            Current = this;
            ServiceStatus = new ServiceStatus(true);

            OrderLoadingCommand = new RelayCommand<Button>(OrderLoadingEvent);
            WindowLoadedCommand = new RelayCommand(WindowLoadedEvent);
            OpenReturnWindow = new RelayCommand<string>(OpenReturnEvent, OpenReturnCanExecute);

            RfidScanner = new RfidScanner();
            WeightScaler = new WeightScaler();

            LoadingView = new Loading();
            IssueViewModel = new IssueViewModel(this);
            LoadingViewModel = new LoadingViewModel(this);
            ItemReturnViewModel = new ItemReturnViewModel(this);
            UILoaderViewModel = new UILoaderViewModel(this);

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += Timer_Tick;
                ActiveView = nameof(ActiveViewE.Loading);
            timer.Start();

            ActiveView = nameof(ActiveViewE.Loading);

        }

        private void OpenReturnEvent(string sender)
        {
            var uiLoader = new UILoader(this);

            ItemReturnViewModel.IsTrayOpened = true;
            ItemReturnViewModel.PreventClosingTray = true;

            if (ActiveView == "Loading" || ActiveView == null)
            {
                ItemReturnViewModel.SelectedTrayNumber = LoadingViewModel.SelectedTrayNumber;
            }
            else if (ActiveView == "Issue")
            {
                //ItemReturnViewModel.SelectedTrayNumber = IssueViewModel.T
            }

            uiLoader.Closing += UILoaderClosing;

            UILoaderViewModel.UILoaderView = new ItemReturn();
            uiLoader.Show();

        }

        private bool OpenReturnCanExecute(string sender)
        {
            return true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if(ActiveView == nameof(ActiveViewE.Loading))
            {
                LoadingViewModel.Timer_Tick(sender, e);
            }else if(ActiveView == nameof(ActiveViewE.Issue))
            {
                IssueViewModel.Timer_Tick(sender, e);
            }else if(ActiveView == nameof(ActiveViewE.ItemReturn))
            {
                ItemReturnViewModel.Timer_Tick(sender, e);
            }
        }

        private async void WindowLoadedEvent()
        {
            //Connect RFID
            await RfidScanner.Connect();

            TrayService = new TrayService(this);
        }

        private void UILoaderClosing(object sender, CancelEventArgs e)
        {
            ItemReturnViewModel.IsTrayOpened = false;
            ItemReturnViewModel.SelectedTrayNumber = 0;
            ItemReturnViewModel.PreventClosingTray = false;
        }

        private void OrderLoadingEvent(Button sender)
        {
            ActiveView = sender.Name;

            IssueViewModel.IsViewLoaded = false;

            switch (sender.Name)
            {
                case nameof(ActiveViewE.Loading) :
                    LoadingView = new Loading();
                    break;
                case nameof(ActiveViewE.Issue):
                    LoadingView = new Issue();
                    IssueViewModel.IsViewLoaded = true;
                    break;
                case nameof(ActiveViewE.Settings):
                    LoadingView = new Setting();
                    break;
                case nameof(ActiveViewE.ItemReturn):
                    LoadingView = new ItemReturn();
                    break;
            }
        }
    }

    public enum ActiveViewE
    {
        Loading,
        Issue,
        Settings,
        ItemReturn
    }
}
