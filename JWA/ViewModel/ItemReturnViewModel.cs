﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using JWA.Helpers;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Enums;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI;
using System.Windows.Input;
using System.Windows.Media;

namespace JWA.ViewModel
{
    public class ItemReturnViewModel : ViewModelBase
    {
        private BaseViewModel _base;

        #region Properties

        public bool _preventClosingTray = false;
        public bool PreventClosingTray
        {
            get
            {
                return _preventClosingTray;
            }
            set
            {
                _preventClosingTray = value;
                RaisePropertyChanged(nameof(PreventClosingTray));
            }
        }

        private List<ArticleCount> _scannedArticle;
        public List<ArticleCount> ScannedArticle
        {
            get
            {
                return _scannedArticle;
            }
            set
            {
                _scannedArticle = value;
                RaisePropertyChanged(nameof(ScannedArticle));
            }
        }

        private ObservableCollection<Article> _articleSearchResult;
        public ObservableCollection<Article> ArticleSearchResult
        {
            get
            {
                return _articleSearchResult;

            }
            set
            {
                _articleSearchResult = value;
                RaisePropertyChanged(nameof(ArticleSearchResult));
            }
        }
        private ObservableCollection<ArticleWithTotal> _articleList;
        public ObservableCollection<ArticleWithTotal> ArticleList
        {
            get
            {
                return _articleList;
            }
            set
            {
                _articleList = value; RaisePropertyChanged(nameof(ArticleList));
            }
        }
        private List<TrayWithTotalArticleQuantity> _articleTrays;
        public List<TrayWithTotalArticleQuantity> ArticleTrays
        {
            get
            {
                return _articleTrays;
            }
            set
            {
                _articleTrays = value;
                RaisePropertyChanged(nameof(ArticleTrays));
            }
        }

        List<int> _trayNumbers;
        public List<int> TrayNumbers
        {
            get => _trayNumbers;
            set
            {
                _trayNumbers = value;
                RaisePropertyChanged(nameof(TrayNumbers));
            }
        }

        int _selectedTrayNumber;
        public int SelectedTrayNumber
        {
            get => _selectedTrayNumber; set
            {
                _selectedTrayNumber = value;
                RaisePropertyChanged(nameof(SelectedTrayNumber));
            }
        }

        double _weight;
        public double Weight
        {
            get => _weight;
            set
            {
                _weight = value;
                RaisePropertyChanged(nameof(Weight));
            }
        }

        private bool _EditableVal1;
        public bool EditableVal1
        {
            get
            {
                return _EditableVal1;
            }
            set
            {
                _EditableVal1 = value;
                RaisePropertyChanged(nameof(EditableVal1));
            }
        }



        public double _articleWeight;
        public double ArticleWeight
        {
            get
            {
                return _articleWeight;
            }
            set
            {
                _articleWeight = value;
                RaisePropertyChanged(nameof(ArticleWeight));
            }
        }

        int _quantity;
        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                RaisePropertyChanged(nameof(Quantity));
            }
        }

        bool _isTrayOpened;
        public bool IsTrayOpened
        {
            get => _isTrayOpened; set
            {
                _isTrayOpened = value; RaisePropertyChanged(nameof(IsTrayOpened));
            }
        }

        public List<string> ScannedArticleRfid;

        private Brush _trayButtonColor;

        internal void Timer_Tick(object seneder, EventArgs e)
        {
            ScanWeightRfidEvent();
        }

        public Brush TrayButtonColor
        {
            get { return _trayButtonColor; }
            set
            {
                _trayButtonColor = value;
                RaisePropertyChanged(nameof(TrayButtonColor));
            }
        }

        string _trayButtonText;
        public string TrayButtonText
        {
            get => _trayButtonText;
            set { _trayButtonText = value; RaisePropertyChanged(nameof(TrayButtonText)); }
        }
        #endregion


        #region Events
        public RelayCommand ScanCommand { get; set; }
        public RelayCommand ClearCommand { get; set; }
        public RelayCommand OpenTrayCommand { get; set; }
        public RelayCommand SubmitScannedCommand { get; set; }
        public RelayCommand<Article> SelectArticleCommand { get; set; }
        public RelayCommand SearchArticleCommand { get; set; }
        public RelayCommand ControlLoadedCommand { get; set; }
        public RelayCommand<TrayWithTotalArticleQuantity> SelectTrayNumber { get; set; }
        #endregion

        public ItemReturnViewModel(BaseViewModel baseViewModel)
        {
            _base = baseViewModel;

            ScanCommand = new RelayCommand(ScanEvent, ScanEventCanExecute);
            ClearCommand = new RelayCommand(ClearEvent, ClearEventCanExecute);
            OpenTrayCommand = new RelayCommand(OpenTrayEvent, OpenTrayCanExecute);
            SubmitScannedCommand = new RelayCommand(SubmitScannedEvent, SubmitScannedExecute);
            SelectArticleCommand = new RelayCommand<Article>(SelectArticleEvent);
            SearchArticleCommand = new RelayCommand(ArticleSearchEvent, ArticleSearchCanExecute);
            ControlLoadedCommand = new RelayCommand(ControlLoadedEvent);
            SelectTrayNumber = new RelayCommand<TrayWithTotalArticleQuantity>(SelectTrayNumberEvent, SelectTrayNumberCanExecute);
            ResetUI();
        }

        private bool SubmitScannedExecute()
        {
            return PreventClosingTray;
        }
        private void ControlLoadedEvent()
        {
            GetArticleList();
        }
        private void SelectTrayNumberEvent(TrayWithTotalArticleQuantity trayWithQuanity)
        {
            SelectedTrayNumber = trayWithQuanity.Tray.Number;
        }

        private bool SelectTrayNumberCanExecute(TrayWithTotalArticleQuantity trayWithQuanity)
        {
            return true;
        }

        public async void GetArticleList()
        {
            ArticleList = await _base.databaseService.GetAllArticleWithTotal();
        }

        private void SubmitScannedEvent()
        {
            SubmitData();
        }
        private bool ArticleSearchCanExecute()
        {
            return (ArticleList != null);
        }

        private bool OpenTrayCanExecute()
        {

            if (IsTrayOpened)
            {
                EditableVal1 = false;
                return !PreventClosingTray;
            }
            else
            {
                EditableVal1 = true;
                return (SelectedTrayNumber != 0 && Quantity != 0 && SelectedArticle != null);
            }

        }
        private string _searchString;
        public string SearchString
        {
            get
            {
                return _searchString;
            }
            set
            {
                _searchString = value;
                RaisePropertyChanged(nameof(SearchString));
            }
        }
        private async void SelectArticleEvent(Article article)
        {
            SelectedArticle = article;
            SearchString = SelectedArticle.Name;

            var tray = await _base.databaseService.GetTrayFromArticle(article.Id);
            ArticleTrays = await _base.databaseService.GetAllTraysFromArticle(article.Id);
            SelectedTrayNumber = tray.Number;

        }

        private void ArticleSearchEvent()
        {
            if (SearchString != null)
            {
                var x = new ObservableCollection<Article>(ArticleList.Where(t => t.Article.Name.ToLower().Contains(SearchString.ToLower())).Select(t => t.Article));

                ArticleSearchResult = (x.Count != ArticleList.Count) ? x : null;
            }

        }

        private Article _selectedArticle;
        public Article SelectedArticle
        {
            get
            {
                return _selectedArticle;
            }
            set
            {
                _selectedArticle = value;
                RaisePropertyChanged(nameof(SelectArticleCommand));
            }
        }

        public int _articleCount;
        public int ArticleCount
        {
            get
            {
                return _articleCount;
            }
            set
            {
                _articleCount = value;
                RaisePropertyChanged(nameof(ArticleCount));
            }
        }


        private async void SubmitData()
        {
            //Add item event
            var trayItemWIthTagsList = new List<TrayItemWithTags>();

            var trayItem = new TrayItemWithTags()
            {
                TrayItem = new TrayItem()
                {
                    ArticleId = SelectedArticle.Id,
                    Date = DateTime.Now,
                    Quantity = Quantity,
                    Weight = ArticleWeight,
                    Status = RequestStatus.Returned,
                    TrayId = SelectedTrayNumber
                },

                ArticleCount = ScannedArticle.Where(t => t.Article.Id == SelectedArticle.Id).FirstOrDefault(),
            };

            trayItemWIthTagsList.Add(trayItem);

            await _base.databaseService.UpdateIssue(trayItemWIthTagsList);

            IsTrayOpened = false;
            ResetUI();

            CommandManager.InvalidateRequerySuggested();
        }

        private void OpenTrayEvent()
        {
            if (IsTrayOpened && ArticleWeight != 0)
            {

                SubmitData();
            }
            else
            {
                TrayButtonText = "Close Tray";
                TrayButtonColor = Brushes.MediumVioletRed;
                IsTrayOpened = true;
            }
        }

        private bool AddItemCanExecute()
        {
            return (!IsTrayOpened);
        }

        private void AddItemEvent()
        {
            throw new NotImplementedException();
        }

        private bool ClearEventCanExecute()
        {
            return (!IsTrayOpened);
        }

        private void ClearEvent()
        {
            ResetUI();
        }

        private async void ScanEvent()
        {
            Weight =  _base.WeightScaler.Read(Weight);

            // var newRfid = _base.RfidScanner.Read();
            // var isValidate = await _base.databaseService.ValidateSameArticles(newRfid);
            //if (isValidate)
            //{
            //    ScannedArticleRfid = new ListThings<string>().RemoveDuplicateAdd(ScannedArticleRfid, newRfid);
            //    ScannedArticle = await _base.databaseService.GetArticleWithTags(ScannedArticleRfid);
            //}

            //select default tray
            if (ScannedArticle != null && ScannedArticle.Count > 0)
            {
                var tray = await _base.databaseService.GetTrayFromArticle(ScannedArticle.First().Article.Id);
                SelectedTrayNumber = tray.Number;
            }
        }

        private bool ScanEventCanExecute()
        {
            return !IsTrayOpened || PreventClosingTray;
        }


        private List<string> _articleRfidList = new List<string>();
        public List<string> ArticleRfidList
        {
            get
            {
                return _articleRfidList;
            }
            set
            {
                _articleRfidList = value;
                RaisePropertyChanged(nameof(ArticleRfidList));
            }
        }

        private async void ScanWeightRfidEvent()
        {
            var weight = _base.WeightScaler.Read(ArticleWeight);

            if (weight != ArticleWeight)
            {
                ArticleWeight = weight;

                var newRfid = _base.RfidScanner.Read();
                //ArticleRfidList = new ListThings<string>().RemoveDuplicateAdd(ArticleRfidList, newRfid);
                ArticleRfidList = newRfid;

                ArticleCount = ArticleRfidList.Count();
            }

        }

        public void ResetUI()
        {
            if (TrayNumbers == null || TrayNumbers.Count == 0)
            {
                TrayNumbers = new List<int>();
                for (var x = 0; x <= 120; x++)
                {
                    TrayNumbers.Add(x);
                }
            }

            SelectedTrayNumber = 0;

            //Weight = 0;
            ArticleWeight = 0;
            Quantity = 0;
            ArticleCount = 0;

            ScannedArticle = new List<ArticleCount>();
            ScannedArticleRfid = new List<string>();

            TrayButtonColor = Brushes.CornflowerBlue;
            TrayButtonText = "Open Tray";

        }


    }
}
