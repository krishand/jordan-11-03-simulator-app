﻿using GalaSoft.MvvmLight;
using System.Windows.Controls;

namespace JWA.ViewModel
{
    public class UILoaderViewModel : ViewModelBase
    {
        public BaseViewModel _Base { get; }

        UserControl _UILoaderView { get; set; }
        public UserControl UILoaderView
        {
            get
            {
                return _UILoaderView;
            }
            set
            {
                _UILoaderView = value;
                RaisePropertyChanged(nameof(UILoaderView));
            }
        }

        public UILoaderViewModel(BaseViewModel _base)
        {
            _Base = _base;
        }


    }
}
