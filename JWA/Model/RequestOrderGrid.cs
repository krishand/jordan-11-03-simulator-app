﻿using SLRS.Jordan.Core.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JWA.Model
{
    public class RequestOrderGrid : RequestOrder
    {
        public Visibility ArticleListGridStatus { get; set; }

        public RequestOrderGrid()
        {
            ArticleListGridStatus = Visibility.Collapsed;
        }
    }
}
