﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWA.Model
{
    public class RfidArticles
    {
        public int ArticleId { get; set; }
        public string RfidTag { get; set; }
    }
}
