﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JWA.ViewModel;
using FontAwesome.WPF;

namespace JWA.Services
{
    public class ServiceStatus
    {
        public Brush BackgroundColor { get; set; }
        public FontAwesomeIcon Icon { get; set; }
        public string Message { get; set; }

        public ServiceStatus(bool useDefault)
        {
            if (useDefault)
            {
                this.StatusReady();
            }
        }

        public void StatusWorking(string message= "")
        {
            Assign(FontAwesomeIcon.ClockOutline, Brushes.MediumVioletRed, message);
        }

        public void StatusReady(string message = "")
        {
            Assign(FontAwesomeIcon.CheckCircle, Brushes.SeaGreen, message);
        }

        public void StatusWarning(string message="")
        {
            Assign(FontAwesomeIcon.Warning, Brushes.Orchid, message);
        }

        private void Assign(FontAwesomeIcon icon, Brush color, string message)
        {
            BackgroundColor = color;
            Icon = icon;
            Message = message;

            BaseViewModel.Current.ServiceStatus = this;
        }

    }
}
