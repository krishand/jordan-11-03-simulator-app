﻿using JWA.Helpers;
using JWA.Model;
using JWA.ViewModel;
using SLRS.Jordan.Core.DTO;
using SLRS.Jordan.Core.Models.Articles;
using SLRS.Jordan.Core.Models.LogiMat;
using SLRS.Jordan.Core.Models.Orders;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWA.Services
{
    public class DatabaseService
    {
        internal async Task<ObservableCollection<RequestOrderGrid>> GetPendingRequestOrdersAsync()
        {
            return await new MyHttpClient<ObservableCollection<RequestOrderGrid>>().GetAsync("/api/orders/pending");
        }

        internal async Task<TrayItemGroupByMachine> GetRequestOrderGroupByMachine(int orderId)
        {
            return await new MyHttpClient<TrayItemGroupByMachine>().GetAsync(string.Format("/api/orders/{0}/machine", orderId));
        }

        public async Task<bool> ValidateSameArticles(List<string> tags)
        {
            //TODO validate this without http call
            var articles = await GetArticleWithTags(tags); 
            //check same article
            if (articles.Count > 1)
            {
                //throw new Exception("RFID with Multiple Article Found");
                BaseViewModel.Current.ServiceStatus.StatusWarning("Multiple RFID found, Please scan again");
                return false;
            }


            return true;
        }

        public async Task<List<ArticleCount>> GetArticleWithTags(List<string> tags)
        {
            var articles = await new MyHttpClient<List<ArticleCount>>().PostAsync("/api/Articles/tags/count/", tags);
            return articles;
        }

        public async Task<ObservableCollection<ArticleWithTotal>> GetAllArticleWithTotal()
        {
            return await new MyHttpClient<ObservableCollection<ArticleWithTotal>>().GetAsync("/api/articles/total");
        }

        internal async Task<int> PostNewArticles(TrayItemWithItemCode trayItem)
        {
            return await new MyHttpClient<int>().PostAsync("/api/trays/create/trayitem", trayItem);
        }

        internal async Task<int> UpdateIssue(List<TrayItemWithTags> trayItemWIthTagsList)
        {
            return await new MyHttpClient<int>().PostAsync("/api/trays/issue/trayitem", trayItemWIthTagsList);
        }

        internal async Task<Tray> GetTrayFromArticle(int articleId)
        {
            var x = await new MyHttpClient<Tray>().GetAsync(string.Format("/api/articles/{0}/tray", articleId));
            return x;
        }

        internal async Task<List<TrayWithTotalArticleQuantity>> GetAllTraysFromArticle(int articleId)
        {
            var x = await new MyHttpClient<List<TrayWithTotalArticleQuantity>>().GetAsync(string.Format("/api/articles/{0}/trays", articleId));
            return x;
        }

        internal async Task<bool> CloseRequest(RequestOrderGrid selectedRequestOrder)
        {
            return await new MyHttpClient<bool>().GetAsync(string.Format("/api/orders/close/{0}", selectedRequestOrder.Id));
        }

        internal async Task<ObservableCollection<Article>> GetAllArticles()
        {
            return await new MyHttpClient<ObservableCollection<Article>>().GetAsync(string.Format("/api/articles/"));
        }
    }
}
