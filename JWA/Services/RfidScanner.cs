﻿using JWA.ViewModel;
using nsAlienRFID2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace JWA.Services
{
    public class RfidScanner
    {
        public bool IsConnected { get; private set; }

        clsReader reader;
        ReaderInfo readerInfo;
        ComInterface readerInterface = ComInterface.enumTCPIP;
        string ip = ConfigurationManager.AppSettings["RfidIp"].ToString();
        string username = ConfigurationManager.AppSettings["RfidUsername"].ToString();
        string password = ConfigurationManager.AppSettings["RfidPassword"].ToString();

        bool isSimulate = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSimulateRfid"]);
        int simulateStartIndex = 0;
        Random rand = new Random();
        private List<string> result;

        public RfidScanner()
        {
            reader = new clsReader(true);
            readerInfo = reader.ReaderSettings;
        }

        private async Task<string> ConnectAsync()
        {
            reader.ReaderSettings = readerInfo;
            reader.InitOnNetwork(ip, 23);
            return await Task.Run(() => reader.Connect());
        }

        public async Task Connect()
        {
            if (!isSimulate)
            {
                BaseViewModel.Current.ServiceStatus.StatusWorking();

                //connecting to the reader
                var result = await ConnectAsync();
                IsConnected = reader.IsConnected;

                if (reader.IsConnected)
                {
                    if (readerInterface == ComInterface.enumTCPIP)
                    {
                        if (!reader.Login(username, password))
                        {
                            BaseViewModel.Current.ServiceStatus.StatusWarning("Wrong RFID Credentials");
                            reader.Disconnect();
                            return;
                        }
                        else
                        {
                            BaseViewModel.Current.ServiceStatus.StatusReady();
                        }
                    }
                }
                else
                {
                    BaseViewModel.Current.ServiceStatus.StatusWarning("Can't connect to RFID");
                }
            }

        }

        public List<string> Read()
        {
            if (!isSimulate)
            {
                if (!IsConnected)
                {
                    throw new Exception("RFID Not Connected!");
                }

                reader.TagListFormat = "XML";
                var tagList = reader.TagList;

                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(tagList);
                var tagLixt = xmlDocument.SelectNodes("Alien-RFID-Tag-List/Alien-RFID-Tag/TagID");

                var result = new List<String>();
                foreach (XmlElement item in tagLixt)
                {
                    result.Add(item.InnerText);
                }

                reader.ClearTagList();
                return result;
            }
            else
            {
                if(result == null)
                {
                    result = new List<String>();
                    var randNext = rand.Next(1, 10);

                    for (var x = simulateStartIndex; x < (simulateStartIndex + randNext); x++)
                    {
                        result.Add(string.Format("0000-{0}", x));
                    }

                    simulateStartIndex += randNext;

                    return result;
                }

                return new List<string>();

            }
        }

        public void Disconnect()
        {
            reader.Disconnect();
        }
    }
}
