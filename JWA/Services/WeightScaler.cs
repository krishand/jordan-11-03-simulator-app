﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWA.Services
{
    public class WeightScaler
    {

        static SerialPort serialPort;
        bool isSimulate = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSimulateRfid"]);
       // bool isSimulate = false;
        Random random;
        private int weight;

        public WeightScaler()
        {
            if (!isSimulate)
            {
                //if (serialPort != null && serialPort.IsOpen)
                //{
                //    serialPort.Close();
                //}

                //serialPort = new SerialPort();
                //serialPort.BaudRate = 4800;
                //serialPort.PortName = "COM8";
                //serialPort.Open();
            }

            random = new Random();
        }
        public void Open()
        {
            //if (serialPort != null && serialPort.IsOpen)
            //{
            //    serialPort.Close();
            //}

            //serialPort = new SerialPort();
            //serialPort.BaudRate = 4800;
            //serialPort.PortName = "COM8";
            //serialPort.Open();
        }

        public double Read(double w)
        {
            double weight = w;

            if (!isSimulate)
            {
                //TODO Fix Readline refresh bug and remove serial opening here 
                if (serialPort != null && serialPort.IsOpen)
                {
                    serialPort.Close();
                }

                serialPort = new SerialPort();
                //serialPort.DataReceived += SerialPort_DataReceived;
                serialPort.BaudRate = 4800;
                serialPort.PortName = "COM3";
                serialPort.Open();
            }
            if (!isSimulate)
            {
                string result = serialPort.ReadLine();
                try
                {
                    weight = Convert.ToDouble(result);
                }
                catch (Exception)
                {
                    weight = 0;
                }
            }
            else
            {
                if (weight < 1)
                {
                    weight = 7;

                }
            }

            if(weight == 0)
            {
                var r = serialPort.ReadLine();
                try
                {
                    weight = Convert.ToDouble(r);
                }
                catch (Exception)
                {
                    weight = w;
                }
            }

            return weight;

        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var x = sender as  SerialPort;
            var y = x.ReadLine();
        }
    }
}
