﻿using JWA.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SLRS.Jordan.WindowsApp.Network
{

    public class TrayService
    {
        //static string connection = "";
        //static string connection2 = "";
        static string telegramType = "";
        static string opening = "";
        static int trayNo = 0;
        public static string rackside = "";

        bool isSimulate = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSimulateTrayCall"]);


        public static string str = "";
       // public static int count = 0;

        BaseViewModel _vm;
        private TcpClient machine1Tcp;
        private TcpClient machine2Tcp;

        public TrayService(BaseViewModel vm)
        {
            if (!isSimulate)
            {

                machine1Tcp = new TcpClient();
                //machine2Tcp = new TcpClient();
                var ip = "127.0.0.1";
               // var ip = "192.168.1.103";
                var port = 2011;
                machine1Tcp.Connect(ip, port);
                //ip = "127.0.0.1";
               // ip = "192.168.1.103";
                //port = 2011;
                //machine2Tcp.Connect(ip, port);

                _vm = vm;
                Thread tid1 = new Thread(new ThreadStart(app1));
                Thread tid2 = new Thread(new ThreadStart(app2));

               // Thread tid3 = new Thread(new ThreadStart(app3));


                tid1.Start();
               // tid3.Start();
                tid2.Start();

                var trayOpen = new List<byte[]>();

            }

        }

        public void app1()
        {

            TcpClient tcpclnt = new TcpClient();
            //connection = "Connecting.....";

            // tcpclnt.Connect("192.168.101.201", 2022);

            //tcpclnt.Connect("192.168.101.201", 2012);
            //tcpclnt.Connect("192.168.80.25", 2022);
            // tcpclnt.Connect("192.168.80.5", 2012);
            // tcpclnt.Connect("192.168.80.5", 2012);
        //tcpclnt.Connect("192.168.1.103", 2012);
                tcpclnt.Connect("127.0.0.1", 2012);
            // use the ipaddress as in the server program

            //connection = "Connected";
            //Console.Write("Enter the string to be transmitted : ");

            //String str = Console.ReadLine();
            Stream stm = tcpclnt.GetStream();

            ASCIIEncoding asen = new ASCIIEncoding();
            //byte[] ba = asen.GetBytes(str);
            //Console.WriteLine("Transmitting.....");

            //stm.Write(ba, 0, ba.Length);
            Stream stm2 = tcpclnt.GetStream();


            while (true)
            {
                _vm.TrayStatus = rackside;


                //Console.WriteLine("stream is :");
                //Console.WriteLine(stm2);

                ASCIIEncoding asen2 = new ASCIIEncoding();


                byte[] bb = new byte[1024];
                stm2.Read(bb, 0, 1024);

                StringBuilder hex = new StringBuilder(bb.Length * 2);
                foreach (byte b in bb)
                    hex.AppendFormat("{0:x2}", b);

                //Console.WriteLine(hex.ToString());
                string str1 = hex.ToString();

                telegramType = str1.Substring(14, 4);

                string responsePacket = str1.Substring(28, 4);
                string counterNo = str1.Substring(8, 2);

                if (responsePacket == "1b63")
                {

                    /*Console.WriteLine("counter Number :" + str2);
                    */
                    string trayno = str1.Substring(50, 2);
                    if (trayno != "00")
                    {
                        trayNo = int.Parse(trayno, System.Globalization.NumberStyles.HexNumber);
                    }

                    string opening1 = str1.Substring(52, 2);
                    if (opening1 == "01")
                    {
                        opening = "on the opening !!";
                    }
                    else
                    {
                        opening = "";
                    }
                    // label1.Text = str4;

                    string rack = str1.Substring(54, 2);
                    // label1.Text = "Rack Side :" + str5;
                    if (rack == "01")
                    {
                        rackside = "front side  !!";
                    }
                    else if (rack == "02")
                    {
                        rackside = "rear side !!";
                    }
                    else if (rack == "03")
                    {
                        rackside = "Elevator !!";
                    }

                }

                if (counterNo == "2f")
                {

                    //string writeword = "000AC9512F020000EF41";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x2F, 0x02, 0x00, 0x00, 0xEF, 0x41 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "30")
                {

                    //string writeword = "000AC95130020000E895";


                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x30, 0x02, 0x00, 0x00, 0xE8, 0x95 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "31")
                {

                    //string writeword = "000AC95131020000E969";


                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x31, 0x02, 0x00, 0x00, 0xE9, 0x69 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "32")
                {

                    //string writeword = "000AC95132020000E92D";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x32, 0x02, 0x00, 0x00, 0xE9, 0x2D };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "33")
                {

                    //string writeword = "000AC95133020000E8D1";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x33, 0x02, 0x00, 0x00, 0xE8, 0xD1 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "34")
                {

                    //string writeword = "000AC95134020000E9A5";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x34, 0x02, 0x00, 0x00, 0xE9, 0xA5 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "35")
                {

                    //string writeword = "000AC95135020000E859";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x35, 0x02, 0x00, 0x00, 0xE8, 0x59 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "36")
                {

                    //string writeword = "000AC95136020000E81D";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x36, 0x02, 0x00, 0x00, 0xE8, 0x1D };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "37")
                {

                    //string writeword = "000AC95137020000E9E1";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x37, 0x02, 0x00, 0x00, 0xE9, 0xE1 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "38")
                {

                    //string writeword = "000AC95138020000EAF5";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x38, 0x02, 0x00, 0x00, 0xEA, 0xF5 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }
                else if (counterNo == "39")
                {

                    //string writeword = "000AC95139020000EB09";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x39, 0x02, 0x00, 0x00, 0xEB, 0x09 };
                    stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }

                else if (counterNo == "00")
                {
                    Console.WriteLine("error incorrect segment recieved !!!");
                    counterNo = "";
                }

                else
                {
                    Console.WriteLine("error incorrect segment recieved !!!");
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x2F, 0x02, 0x00, 0x00, 0xEF, 0x41 };
                    // stm2.Write(ba, 0, ba.Length);
                    counterNo = "";
                }

            
                System.Threading.Thread.Sleep(100);
                Thread.Yield();
                //Application.DoEvents();
                GC.Collect();

                
                // for (int i = 1; i <= 10; i++)
                // {
                //Form1 frm2 = new Form1();
                //  var1 = "Surname";
                //frm2.lblstatus.Text = "app1";
                //   Console.Write("lol");
                // Thread.Yield();
                //Application.DoEvents();
                //GC.Collect();
                
            }
        }

        public void app3()
        {

            TcpClient tcpclnt = new TcpClient();
            //connection = "Connecting.....";

            // tcpclnt.Connect("192.168.101.201", 2022);

            //tcpclnt.Connect("192.168.101.201", 2012);
            //tcpclnt.Connect("192.168.80.25", 2022);
      // tcpclnt.Connect("192.168.1.103", 2022);
            //     tcpclnt.Connect("192.168.80.5", 2012);
            // tcpclnt.Connect("192.168.80.5", 2012);
            tcpclnt.Connect("127.0.0.1", 2022);
            // use the ipaddress as in the server program

            //connection = "Connected";
            //Console.Write("Enter the string to be transmitted : ");

            //String str = Console.ReadLine();
            Stream stm = tcpclnt.GetStream();

            ASCIIEncoding asen = new ASCIIEncoding();
            //byte[] ba = asen.GetBytes(str);
            //Console.WriteLine("Transmitting.....");

            //stm.Write(ba, 0, ba.Length);
            Stream stm2 = tcpclnt.GetStream();


            while (true)
            {

                //Console.WriteLine("stream is :");
                //Console.WriteLine(stm2);

                ASCIIEncoding asen2 = new ASCIIEncoding();


                byte[] bb = new byte[1024];
                stm2.Read(bb, 0, 1024);

                StringBuilder hex = new StringBuilder(bb.Length * 2);
                foreach (byte b in bb)
                    hex.AppendFormat("{0:x2}", b);

                //Console.WriteLine(hex.ToString());
                string str1 = hex.ToString();


                string responsePacket = str1.Substring(28, 4);
                string counterNo = str1.Substring(8, 2);

                if (responsePacket == "1b63")
                {


                    /*Console.WriteLine("counter Number :" + str2);
                    */
                    string trayno = str1.Substring(50, 2);
                    if (trayno != "00")
                    {
                        trayNo = int.Parse(trayno, System.Globalization.NumberStyles.HexNumber);
                    }

                    string opening1 = str1.Substring(52, 2);
                    if (opening1 == "01")
                    {
                        //opening = "on the opening !!";
                    }
                    else
                    {
                        //opening = "";
                    }
                    // label1.Text = str4;

                    string rack = str1.Substring(54, 2);
                    // label1.Text = "Rack Side :" + str5;
                    if (rack == "01")
                    {
                        rackside = "front side  !!";
                    }
                    else if (rack == "02")
                    {
                        rackside = "rear side !!";
                    }
                    else if (rack == "03")
                    {
                        rackside = "Elevator !!";
                    }

                }

                if (counterNo == "2f")
                {

                    //string writeword = "000AC9512F020000EF41";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x2F, 0x02, 0x00, 0x00, 0xEF, 0x41 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "30")
                {

                    //string writeword = "000AC95130020000E895";


                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x30, 0x02, 0x00, 0x00, 0xE8, 0x95 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "31")
                {

                    //string writeword = "000AC95131020000E969";


                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x31, 0x02, 0x00, 0x00, 0xE9, 0x69 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "32")
                {

                    //string writeword = "198973879292585372425";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x32, 0x02, 0x00, 0x00, 0xE9, 0x2D };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "33")
                {

                    //string writeword = "000AC95133020000E8D1";

                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x33, 0x02, 0x00, 0x00, 0xE8, 0xD1 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "34")
                {

                    //string writeword = "000AC95134020000E9A5";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x34, 0x02, 0x00, 0x00, 0xE9, 0xA5 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "35")
                {

                    //string writeword = "000AC95135020000E859";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x35, 0x02, 0x00, 0x00, 0xE8, 0x59 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "36")
                {

                    //string writeword = "000AC95136020000E81D";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x36, 0x02, 0x00, 0x00, 0xE8, 0x1D };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "37")
                {

                    //string writeword = "000AC95137020000E9E1";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x37, 0x02, 0x00, 0x00, 0xE9, 0xE1 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "38")
                {

                    //string writeword = "000AC95138020000EAF5";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x38, 0x02, 0x00, 0x00, 0xEA, 0xF5 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "39")
                {

                    //string writeword = "000AC95139020000EB09";
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x39, 0x02, 0x00, 0x00, 0xEB, 0x09 };
                    stm2.Write(ba, 0, ba.Length);
                }
                else if (counterNo == "00")
                {

                }
                else
                {
                    Console.WriteLine("error incorrect segment recieved !!!");
                    byte[] ba = new byte[] { 0x00, 0x0A, 0xC9, 0x51, 0x2F, 0x02, 0x00, 0x00, 0xEF, 0x41 };
                    stm2.Write(ba, 0, ba.Length);
                }

                System.Threading.Thread.Sleep(100);
                Thread.Yield();
                //Application.DoEvents();
                GC.Collect();

            }
        }

        public bool OpenTray(int trayId)
        {
            if (rackside != "Elevator !!" && opening != "on the opening !!" && telegramType != "1b7d")
            {
                //if (isSimulate)
                //{
                //    return;
                //}
                var machine = (trayId > 60) ? 2 : 1;
                var trayNumber = trayId - (60 * (machine - 1));

                var trayHex = (byte)Convert.ToInt32(string.Format("0x{0:X}", trayNumber), 16);

                var byteArr = new byte[] { 0x00, 0x2f, 0x66, 0x44, 0x31, 0x02, 0x00, 0x00, 0x88, 0x37,
                0x00, 0xc9, 0x00, 0x66, 0x1b, 0x62, 0x00, 0x01, 0x00, 0x00,
                0x00, 0x0a, 0x00, 0x01, 0x00, trayHex, 0x01, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x64, 0x64, 0x64, 0x64,
                0x64, 0x64, 0x00, 0x00, 0x00, 0x00, 0x00 };

                Stream stm = null;

                //if (machine == 1)
                //{
                //    stm = machine2Tcp.GetStream();
                //}
                //else if(machine == 2)
                //{
                //    c
                //}

                stm = machine1Tcp.GetStream();

                stm.Write(byteArr, 0, byteArr.Length);

                return true;
            }
            else
            {
                System.Windows.MessageBox.Show("Logimat Error Cannot call Trays");
                return false;
            }
        }

        public bool CloseTray(int trayId)
        {
            //if (isSimulate)
            //{
            //    return;
            //}
            if (opening == "on the opening !!" && telegramType != "1b7d")
            {
                var machine = (trayId > 60) ? 2 : 1;
                var trayNumber = trayId - (60 * (machine - 1));

                if (!isSimulate)
                {
                    var trayHex = (byte)Convert.ToInt32(string.Format("0x{0:X}", trayNumber), 16);

                    var byteArr = new byte[] { 0x00, 0x2f, 0x66, 0x44, 0x32, 0x02, 0x00,
                0x00, 0x88, 0x73, 0x00, 0xc9, 0x00, 0x66, 0x1b, 0x62, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x0b, 0x00, 0x01, 0x00, trayHex, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x64, 0x64, 0x64, 0x64,
                0x64, 0x64, 0x00, 0x00, 0x00, 0x00, 0x00};

                    Stream stm = null;

                    //if (machine == 1)
                    //{
                    //    stm = machine2Tcp.GetStream();
                    //}
                    //else if (machine == 2)
                    //{
                    //    stm = machine1Tcp.GetStream();

                    //}
                    stm = machine1Tcp.GetStream();


                    stm.Write(byteArr, 0, byteArr.Length);
                }
                return true;
            }
            else
            {
                System.Windows.MessageBox.Show("Logimat Error Cannot return Trays");

                return false;
            }

        }


        public void app2()
        {

            TcpClient tcpclnt1 = new TcpClient();
            //TcpClient tcpclnt2 = new TcpClient();

            //Console.WriteLine("Connecting.....");

           // tcpclnt1.Connect("192.168.101.201", 2021);

           //tcpclnt2.Connect("192.168.101.201", 2011);
            //tcpclnt1.Connect("192.168.101.201", 2011); //machine2       
            //tcpclnt2.Connect("192.168.101.201", 2021); //machine1
            //tcpclnt1.Connect("192.168.80.25", 2021);
            //tcpclnt2.Connect("192.168.80.5", 2011);
            // tcpclnt.Connect("127.0.0.1", 2011);
            // use the ipaddress as in the server program

            int i = 0;


            //connection2 = "connected !!!";
            //Console.WriteLine("Connected");
                      while (true)
                      {
                          //  Console.Write("Enter the Command (1..5,22) (a..e,z) (Enter for keep alive) : ");

                              Stream stm1 = machine1Tcp.GetStream();
                              //Stream stm2 = machine2Tcp.GetStream();

                              byte[] ba1 = null;

                              switch (i)
                              {
                                  case 0:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x30, 0x02, 0x00, 0x00, 0xFD, 0x3A };
                                      i++;
                                      break;
                                  case 1:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x31, 0x02, 0x00, 0x00, 0xFC, 0xC6 };
                                      i++;
                                      break;

                                  case 2:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x32, 0x02, 0x00, 0x00, 0xFC, 0x82 };
                                      i++;
                                      break;

                                  case 3:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x33, 0x02, 0x00, 0x00, 0xFD, 0x7E };
                                      i++;
                                      break;
                                  case 4:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x34, 0x02, 0x00, 0x00, 0xFC, 0x0A };
                                      i++;
                                      break;
                                  case 5:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x35, 0x02, 0x00, 0x00, 0xFD, 0xF6 };
                                      i++;
                                      break;
                                  case 6:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x36, 0x02, 0x00, 0x00, 0xFD, 0xB2 };
                                      i++;
                                      break;
                                  case 7:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x37, 0x02, 0x00, 0x00, 0xFC, 0x4E };
                                      i++;
                                      break;
                                  case 8:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x38, 0x02, 0x00, 0x00, 0xFF, 0x5A };
                                      i++;
                                      break;
                                  case 9:
                                      ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x39, 0x02, 0x00, 0x00, 0xFE, 0xA6 };
                                      i = 0;
                                      break;
                              }

                              //Console.WriteLine(ba1[9]);
                              //byte[] ba1 = new byte[] { 0x00, 0x0a, 0x65, 0x44, 0x30, 0x02, 0x00, 0x00, 0xFD, 0x3A };
                              stm1.Write(ba1, 0, ba1.Length);
                              //stm2.Write(ba1, 0, ba1.Length);
                          System.Threading.Thread.Sleep(3000);
                          Thread.Yield();
                          GC.Collect();

                      }

            System.Threading.Thread.Sleep(4000);
                Thread.Yield();
                // Application.DoEvents();
                GC.Collect();
            }

        }
    }


